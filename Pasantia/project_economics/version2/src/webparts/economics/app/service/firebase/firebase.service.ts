import { CryptoTls } from './../../utilities/crypto';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { first, map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireRemoteConfig } from '@angular/fire/remote-config';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  protected getConfigurationObject: any = null;

  constructor(private firestore: AngularFirestore, private remoteConfig: AngularFireRemoteConfig) {
    this.loadConfiguration();
  }

  public getItemsFromCollection(collection: string, valueOfFind: string): Observable<any> {
    return this.firestore.collection(collection).doc(valueOfFind).snapshotChanges().pipe(
      map((doc: any) => {
        const data = doc.payload.data();
        const id = doc.payload.id;
        return (data) ? { id, ...data } : undefined;
      }));
  }

  public getAllItemsFromCollection(collection: string): Observable<any> {
    console.log('collection ', collection);
    return this.firestore.collection(collection).snapshotChanges()
    .pipe(
      map((snaps) =>
        snaps.map((snap) => {
          return {
            id: snap.payload.doc.id,
            ...(snap.payload.doc.data() as {})
          };
        })
      ),
      first()
    );
  }

  public getRemoteConfig(profile: string): Promise<any> {
   return new Promise ( (resolve , reject) => {
     let valueToReturn = '';
     const session = sessionStorage.getItem(environment.session.data) || '';
     if (this.getConfigurationObject === null && session === '') {
      reject('error loading routes');
     } else {
       if (this.getConfigurationObject) {
        valueToReturn = this.getConfigurationObject[profile];
        sessionStorage.setItem(environment.session.data , CryptoTls.encode( valueToReturn ));
       } else {
        valueToReturn = CryptoTls.decode(session);
       }
     }
     resolve( JSON.parse(valueToReturn));
    } );
  }

  private loadConfiguration(): void {
    this.remoteConfig.fetchAndActivate().then(() => {
      this.remoteConfig.strings.subscribe((objeto) => {
        if ( objeto && Object.keys(objeto).length > 0 ) {
          this.getConfigurationObject = objeto ;
        }
        this.validateStatus();
      }, (error) => console.error(error) );
    } , (error) => console.error(error) );
  }


  private validateStatus(): void {
    this.remoteConfig.lastFetchStatus.then(status => {
      console.log('Estado del Remote Config ', status);
    });
  }

  public getConfiguration(): any {
    return this.getConfigurationObject;
  }
}
