import jwtDecode from "jwt-decode";

export default class Jwt {

    public static decode( token: string ): any {
        return jwtDecode(token);
    }
}