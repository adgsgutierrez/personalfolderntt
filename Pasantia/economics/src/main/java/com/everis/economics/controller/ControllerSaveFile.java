package com.everis.economics.controller;

import com.everis.economics.beans.UploadFileResponse;
import com.everis.economics.service.ServiceSaveFile;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;

@RestController
public class ControllerSaveFile {

    @Autowired
    private ServiceSaveFile storageService;

    @Autowired
    private UploadFileResponse response;

    @RequestMapping(value = "/api/fileupload", method = RequestMethod.POST)
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) throws IOException, OpenXML4JException {
        String fileName = storageService.storeFile(file);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(fileName)
                .toUriString();

        response.setFileName(fileName);
        response.setFileDownloadUri(fileDownloadUri);
        response.setFileType(file.getContentType());
        response.setSize(file.getSize());
        return response;
    }

    @RequestMapping(value = "/api/fileupload", method = RequestMethod.GET)
    public String uploadFile(){
        return "Holaaa";
    }
}
