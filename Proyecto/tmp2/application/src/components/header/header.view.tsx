import React from 'react';
import { Text } from 'galio-framework';
import {  Button, Image, View } from 'react-native';
import styles from './header.style';

export default class HeaderApplication extends React.Component{

    constructor(props) {
        super(props);
    }

    

    render(){
        const children: any = this.props;
        return (
            <View style={styles.containerheader}>
                <Image source={children.icon} style={styles.imageHeader}/>
                <Text style={styles.labelButton}>{ children.label}</Text>
            </View>
        );
    }
}