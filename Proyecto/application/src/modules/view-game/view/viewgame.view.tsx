import React from 'react';
import { Alert , Text , NativeModules } from 'react-native';
import { Block } from 'galio-framework';
import AsyncStorage from "@react-native-community/async-storage";
import HeaderApplication from '../../../components/header/header.view';
import ViewGamePresenter from '../presenter/viewgame.presenter';

export default class ViewGame extends React.Component{

    private presenter: ViewGamePresenter = new ViewGamePresenter();

    componentDidMount() {
        let game = '0';      
        AsyncStorage.getItem('feature').then(( value ) => {
            if ('Agente' == value ){
                game = '2' ;
            }
            else if ('Alentador' == value ){
                game = '2' ;
            }
            else if ('Consejero' == value ){
                game = '1' ;
            }
            else if ('Creativo' == value ){
                game = '3' ;
            }
            else if ('Especialista' == value ){
                game = '1' ;
            }
            else if ('Evaluador' == value ){
                game = '2' ;
            }
            else if ('Investigador' == value ){
                game = '3' ;
            }
            else if ('Objetivo' == value ){
                game = '3' ;
            }
            else if ('OrientadoResultados' == value ){
                game = '3' ;
            }
            else if ('Perfeccionista' == value ){
                game = '1' ;
            }
            else if ('Persuasivo' == value ){
                game = '2' ;
            }
            else if ('Profesional' == value ){
                game = '1' ;
            }
            else if ('Promotor' == value ){
                game = '2' ;
            }
            else if ('Realizador' == value ){
                game = '1' ;
            }
            else if ('Resolutivo' == value ){
                game = '3' ;
            }

            NativeModules.GameModule.NavigateToGame(game);   
        });

         
    }

    render () {
        // @ts-expect-error
        const { navigation } = this.props;

        return (
        <Block flex>
             <HeaderApplication 
                    icon={require('./../../../../assets/images/game.png')}
                    label="Habilidad"></HeaderApplication>
                <Text>Loading...</Text>
        </Block>
        );
    }
}