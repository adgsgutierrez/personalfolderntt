package co.com.konrad.police_game.controller.auth;

import co.com.konrad.police_game.entity.Auth;
import co.com.konrad.police_game.entity.Response;
import co.com.konrad.police_game.service.auth.IService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@Slf4j
public class AuthController implements IController {

    @Autowired
    @Qualifier("AuthServiceImpl")
    private IService service;

    @Override
    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public ResponseEntity<Response> getAuthorization(@RequestBody Auth input) throws Exception {
        log.info("Input" + input.toString());
        return ResponseEntity.ok(service.getAuthorization(input));
    }
}
