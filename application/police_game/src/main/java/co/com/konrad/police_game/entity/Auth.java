package co.com.konrad.police_game.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@ToString
public class Auth implements Serializable {

    @Setter
    @Getter
    @JsonProperty("user")
    private String user;

    @Setter
    @Getter
    @JsonProperty("password")
    private String password;

    @Override
    public String toString() {
        return String.format("{ \"user\" : \"%s\" , \"password\" : \"%s\" }", user, password);
    }

}
