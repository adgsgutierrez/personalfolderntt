import React from 'react';
import { StatusBar, Image } from 'react-native';
import { Block } from 'galio-framework';
import styles from './menu.style';
import { Images } from '../../../../assets/Images';
import ItemMenu from '../../../components/item-menu/item_menu.view';
import MenuPresenter from '../presenter/menu.presenter';

export default class Menu extends React.Component{

    private menuPresenter = new MenuPresenter();

    render () {
        // @ts-expect-error
        const { navigation } = this.props;

        return (
        <Block flex>
            <StatusBar barStyle="light-content" />
            <Block flex style={styles.containerBanner}>
                <Image source={ Images.imageBanner } style={styles.imageBanner}/>
            </Block>
            <Block flex style={styles.containerMenu}>
               <ItemMenu 
                    navigateTo={ () => { this.menuPresenter.sendTestBook(navigation) } } 
                    icon={require('./../../../../assets/images/alegoria.png')}
                    label="Alegoría">
                </ItemMenu>
               <ItemMenu 
                    navigateTo={ () => {this.menuPresenter.sendHability(navigation) } } 
                    icon={require('./../../../../assets/images/game.png')}
                    label="Habilidad">
                </ItemMenu>
               <ItemMenu 
                    navigateTo={ () => {this.menuPresenter.sendProfile(navigation) } } 
                    icon={require('./../../../../assets/images/profile.png')}
                    label="Perfil">
                </ItemMenu>
                <Image source={ Images.imageLogo } style={styles.imageFooter}/>
            </Block>
        </Block>
        );
    }
}