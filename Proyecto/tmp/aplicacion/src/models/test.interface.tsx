export interface ITest{
    test_id : number;
    test_user : number;
    test_total_d : number;
    test_total_i : number;
    test_total_s : number;
    test_total_c : number;
    test_total : number;
    test_create : Date;
    test_update : Date;
}