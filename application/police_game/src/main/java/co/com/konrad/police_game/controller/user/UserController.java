package co.com.konrad.police_game.controller.user;

import co.com.konrad.police_game.entity.Response;
import co.com.konrad.police_game.entity.User;
import co.com.konrad.police_game.service.user.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserController implements IController {

    @Autowired
    @Qualifier("UserServiceImpl")
    private IService service;

    @Override
    @PostMapping
    public Response search( @RequestBody User user) {
        return service.findUser(user);
    }
}
