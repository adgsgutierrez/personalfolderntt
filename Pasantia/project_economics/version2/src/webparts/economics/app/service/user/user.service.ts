import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { FirebaseService } from '../firebase/firebase.service';
import { IUser } from './../../interface/user.interface';
import { COLLECTIONS } from './../../utilities/constantes';
import { Utils } from './../../utilities/functions';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private firebase: FirebaseService) { }

  public searchCode(code: string): Promise<IUser> {
    return new Promise(
      ( resolve , reject ) => {
        this.firebase.getItemsFromCollection( COLLECTIONS.USER , code ).subscribe(
          (data) => {
            if (data) {
              resolve(data);
            } else {
              reject();
            }
          }
        );
      }
    );
  }

  public searchMenuUser(): Promise<any> {
    const user: IUser = Utils.getDataInSessionStorage();
    return this.firebase.getRemoteConfig(user.profile);
  }

  public getAllUsers(): Observable<IUser[]>{
    return this.firebase.getAllItemsFromCollection(COLLECTIONS.USER);
  }
}
