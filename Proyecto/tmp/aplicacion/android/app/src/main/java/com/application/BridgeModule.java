package com.application;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.unity3d.player.UnityPlayerActivity;

public class BridgeModule extends ReactContextBaseJavaModule {

    private ReactApplicationContext context = getReactApplicationContext();

    public BridgeModule(@Nullable ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @NonNull
    @Override
    public String getName() {
        return "GameModule";
    }

    @ReactMethod
    public void NavigateToGame(){
        Intent intent = new Intent(context , UnityPlayerActivity.class);
        if(intent.resolveActivity(context.getPackageManager()) != null ){
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
}
