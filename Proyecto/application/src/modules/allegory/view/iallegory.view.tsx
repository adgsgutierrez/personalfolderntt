import { IAnswer } from "../../../models/question.interface";

export interface IAllegoryView{
    refreshData() : void;
    viewResults(answers: IAnswer) : void;
}