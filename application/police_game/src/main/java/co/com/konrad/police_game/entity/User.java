package co.com.konrad.police_game.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.util.Base64Utils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Date;

@Slf4j
@Entity
@Table(name = "user")
public class User implements Serializable {

    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_id")
    @JsonProperty("user_id")
    private Integer user_id;

    @Getter
    @Setter
    @Column(name="user_name")
    @JsonProperty("user_name")
    private String user_name;

    @Getter
    @Setter
    @Column(name="user_lastname")
    @JsonProperty("user_lastname")
    private String user_lastname;

    @Getter
    @Setter
    @Column(name="user_range")
    @JsonProperty("user_range")
    private String user_range;

    @Getter
    @Setter
    @Column(name="user_create")
    @JsonProperty("user_create")
    private Date user_create;

    @Getter
    @Setter
    @Column(name="user_update")
    @JsonProperty("user_update")
    private Date user_update;

    @Getter
    @Setter
    @Column(name="user_user")
    @JsonProperty("user_user")
    private String user_user;

    @Getter
    @Setter
    @Column(name="user_gender")
    @JsonProperty("user_gender")
    private String user_gender;

    @Setter
    @Column(name="user_password")
    @JsonProperty("user_password")
    private String user_password;

    public String getUser_password() {
        byte[] decodedBytes = Base64.decodeBase64(user_password.getBytes());
        user_password = new String(decodedBytes);
        for (int index = 0; index < 2; index++) {
            user_password = user_password.substring(1);
            decodedBytes = Base64.decodeBase64(user_password.getBytes());
            user_password = new String(decodedBytes);
        }
        return user_password;
    }

    @Override
    public String toString() {
        return String.format("{ \"user_id\" : \"%s\" , \"user_user\" : \"%s\" , \"user_name\" : \"%s\" ," +
                " \"user_lastname\" : \"%s\" , \"user_range\" : \"%s\" , \"user_gender\" : \"%s\" }",
                user_id , user_user, user_name , user_lastname , user_range , user_gender);
    }
}
