import {  StyleSheet, Dimensions } from 'react-native';
import Theme from '../../../assets/Theme';

const { width , height } = Dimensions.get('screen');

const widthDimen = width - 40;
const widthDimenLabel = width - 150;

export default StyleSheet.create({
    containerButton: {
      height: 90,
      paddingLeft:20,
      borderRadius: 20,
      marginBottom:20,
      width: widthDimen,
//   justifyContent: 'center', 
      alignItems: 'center',
      flexDirection: "row",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      elevation: 4,
      backgroundColor: Theme.COLORS.WHITE
      
    },
    imageButton: {
      height: 60,
      width: 60
    },
    labelButton: {
        width :widthDimenLabel,
        textAlign: "center",
        color: Theme.COLORS.GREEN,
        fontSize: 20
    }
  });