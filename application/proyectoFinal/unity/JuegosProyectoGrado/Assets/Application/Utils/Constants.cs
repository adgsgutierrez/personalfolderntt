﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants 
{
    private const string ROUTE_PATH = "configuration/";
    public const string FILE_TMP = ROUTE_PATH + "investigador";

    public const string PROFILE_FOR_USE = "USE_THIS_PROFILE";
}
