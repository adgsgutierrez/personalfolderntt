import AsyncStorage from "@react-native-community/async-storage";
import { IResponse } from "../../../models/response.interface";
import { IUser } from "../../../models/user.interface";
import { Constants } from "../../../utilities/constants";
import { Functions } from "../../../utilities/functions";
import ServiceHttp from "../../../utilities/service"

export default class LoginModel {

    private service: ServiceHttp = new ServiceHttp();

    public LoginApp( userInput , pwdInput ): Promise<IResponse>{
        const user: IUser = {
            user_id : null,
            user_create : null,
            user_lastname: null,
            user_name: null,
            user_range: null,
            user_update: null,
            user_user: userInput,
            user_password: Functions.encriptData(pwdInput)
        }
        const host = Constants.HOST + Constants.PATH.LOGIN;
        return this.service.servicePost(host , user);
    }

    public SearchLocalData(): Promise<any>{
        return AsyncStorage.getItem(Constants.PERSISTENCE.USER)
    }
}