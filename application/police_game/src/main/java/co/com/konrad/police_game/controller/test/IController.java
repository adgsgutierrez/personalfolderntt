package co.com.konrad.police_game.controller.test;

import co.com.konrad.police_game.entity.Response;
import co.com.konrad.police_game.entity.Test;

public interface IController {

    Response setTest(Test test);

}
