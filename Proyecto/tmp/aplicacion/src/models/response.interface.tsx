export interface IResponse{
    code: number;
    message: string;
    data: any;
}

export interface IResponseConfiguration{
    profile: string;
    items: number[];
}