package co.com.konrad.police_game.service.user;

import co.com.konrad.police_game.entity.Response;
import co.com.konrad.police_game.entity.User;
import co.com.konrad.police_game.repository.UserRepository;
import co.com.konrad.police_game.security.JwtSecurity;
import co.com.konrad.police_game.utils.Constants;
import co.com.konrad.police_game.utils.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

@Slf4j
@Service
@Qualifier("UserServiceImpl")
public class UserServiceImpl implements IService {

    @Autowired
    private Response response;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private Environment env;

    @Autowired
    private EntityManagerFactory emf;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    public Response findUser(User user) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<User> query = (TypedQuery<User>) em.createQuery( Constants.QUERY_SEARCH_USER );
        User userResult = null;
        try {
            userResult = query.setParameter("user", user.getUser_user()).setParameter("pwd" , user.getUser_password()).getSingleResult();
        } catch (NoResultException e){
            log.error(e.getMessage());
        }
        response.setMessage( (userResult != null)? Constants.SUCCESS_SMS : Constants.NO_DATA_SMS );
        response.setCode( (userResult != null)? Constants.SUCCESS_CODE: Constants.NO_DATA_CODE);
        response.setData( (userResult != null)? jwtTokenUtil.encriptJwt(userResult.toString()) : "No data");
        return response;
    }
}
