import { IService } from '../i.service';
import { Xlsx } from './../../utilities/read.xls';

export class ServiceGeco implements IService {

    private static files: string[] = [
        'EXT-012426-00068 - DX- Evolución Canales Telefónica - 20210112.xlsb',
        'EXT-012426-00068 - DX- Evolución Canales Telefónica - 20201221.xlsb',
        'EXT-012426-00068 - DX- Evolución Canales Telefónica - 20200824.xlsb',
        'GECO.xlsb',
        'EXT-014647-00041 Enel App Movil 20201022.xlsb',
        'ZPS_F_ACTUAL_LINE_ITEMSSet (15).xlsx'
    ];

    sendService(request: any, response: any) {
        const res = { result : '', sms: '' };
        try {
            const route = `${__dirname}/files/`+ ServiceGeco.files[request.param("id")];
            res.sms = 'with file ' +route;
            const result = Xlsx.read(route);
            console.log(result);
            res.result = 'ok';
        } catch (err) {
            console.error('err' , err);
            res.result = 'error';
        }
        response.send(res);
    }

}