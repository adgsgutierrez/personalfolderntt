export interface IUser {
    user_id : number;
    user_name : string;
    user_lastname : string;
    user_range : string;
    user_create : Date;
    user_update : Date;
    user_user : string;
    user_password : string;
    user_gender: string;
}