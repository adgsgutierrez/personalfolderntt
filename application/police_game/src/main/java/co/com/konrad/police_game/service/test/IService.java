package co.com.konrad.police_game.service.test;

import co.com.konrad.police_game.entity.Response;
import co.com.konrad.police_game.entity.Test;

import java.util.Optional;

public interface IService {

    Response save(Test test);

}
