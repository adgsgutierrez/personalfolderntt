import {  StyleSheet, Dimensions } from 'react-native';
import Theme from '../../../../assets/Theme';

const { width , height } = Dimensions.get('screen');

export default StyleSheet.create({
    container: {
      height: '100%',
      width: '100%'
    },
    textTitle: {
      fontSize: 33,
      marginTop: 40,
      width : width,
      textTransform: "uppercase",
      textAlign: "center",
      color: Theme.COLORS.GREEN
    },
    textSubTitle: {
      fontSize: 20,
      marginTop: 40,
      width : width,
      textAlign: "center",
      color: Theme.COLORS.GREEN
    },
    containerButtons: {
      flex: 1,
      marginTop: 30,
      flexDirection:'row',
    },
    containerColumButtons: {
      flex: 1,
      flexDirection:'column',
      padding: 12
    },
    button: {
      height: 150,
      marginBottom:20,
      alignContent: "center",
      borderRadius: 15,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Theme.COLORS.ACTIVE
    },
    textButton: {
      fontSize: 20,
      color: Theme.COLORS.WHITE,
      textAlign: "center",
      textTransform: 'capitalize'
    },
    imageFooter: {
      width:40,
      height:40,
    }
  });