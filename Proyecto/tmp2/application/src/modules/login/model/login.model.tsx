import AsyncStorage from "@react-native-community/async-storage";
import { IResponse } from "../../../models/response.interface";
import { IUser } from "../../../models/user.interface";
import { Constants } from "../../../utilities/constants";
import { Functions } from "../../../utilities/functions";
import ServiceHttp from "../../../utilities/service"

export default class LoginModel {

    private service: ServiceHttp = new ServiceHttp();

    public LoginApp( userInput , pwdInput ): Promise<IResponse>{
        
        const host = Constants.HOST + Constants.PATH.LOGIN;
        //return this.service.servicePost(host , user);
        return new Promise((res)=>{
            const user: IUser = {
                user_id : 1,
                user_create : new Date(),
                user_lastname: 'Lopez',
                user_name: 'Pedro',
                user_range: 'Mayor',
                user_update: new Date(),
                user_gender: 'F',
                user_user: userInput,
                user_password: Functions.encriptData(pwdInput)
            }
             const response: IResponse = {
                 code: Constants.STATUS.SUCCESS,
                 message: '',
                 data: user
             }
            res(response);
        });
    }

    public SearchLocalData(): Promise<any>{
        return AsyncStorage.getItem(Constants.PERSISTENCE.USER)
    }
}