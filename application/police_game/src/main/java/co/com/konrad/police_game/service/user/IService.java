package co.com.konrad.police_game.service.user;

import co.com.konrad.police_game.entity.Response;
import co.com.konrad.police_game.entity.User;


public interface IService {

    Response findUser(User user);

}
