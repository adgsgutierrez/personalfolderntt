import React, { useState } from 'react';
import { Text, TextInput, View } from 'react-native';
import { StatusBar , Image , Dimensions } from 'react-native';
import { Block, Input , Button } from 'galio-framework';
import styles from './login.styles';
import LoginPresenter from '../presenter/login.presenter';
import {Images} from './../../../../assets/Images';
import Theme from '../../../../assets/Theme';
import { Constants } from '../../../utilities/constants';
const { height, width } = Dimensions.get('screen');

export default class LoginView extends React.Component {

    private loginPresenter = new LoginPresenter();

    private navigate: any;

    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            userPassword: ''
        } 
    }
    componentDidMount() {
        const { navigation } = this.props;
        this.navigate = navigation;

        if(Constants.ISOFFLINE){
            this.loginPresenter.navigateToMenu(this.navigate);
        }

        this.loginPresenter.validateTologin().then(
            (isLogin: boolean) => {
                if (isLogin) {
                    this.loginPresenter.navigateToMenu(this.navigate);
                }
            }
        )
    }

    render() {
        return (
            <Block flex style={styles.container}>
                <StatusBar barStyle="light-content" />
                <Block style={styles.containerIcon}>
                    <Image source={ Images.imageApp } style={styles.imageHeader}/>
                </Block>
                <Block style={styles.containerCard}>
                    <Text style={styles.textLabel}>¿Que tanto te conoces?</Text>
                    <Input placeholderTextColor="#4b9463" style={styles.inputForm} placeholder="Usuario" onChangeText={userName => this.setState({userName})} />
                    <Input placeholderTextColor="#4b9463" password style={styles.inputForm} placeholder="Clave" onChangeText={userPassword => this.setState({userPassword})} />
                    <Button onPress={() => { this.loginPresenter.sendToLogin(this.navigate , this.state) }} style={styles.button} >Iniciar Sesión</Button>
                    <Image source={ Images.imageLogo } style={styles.imageFooter}/>
                </Block>
            </Block>
        );
    }
}
