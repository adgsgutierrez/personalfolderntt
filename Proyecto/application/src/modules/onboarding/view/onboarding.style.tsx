import { StyleSheet } from 'react-native';
import Theme from './../../../../assets/Theme';

export default StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: Theme.COLORS.GREEN,
        marginTop: 0,
        marginBottom: 0,
        marginLeft: 0,
        marginRight: 0,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageLogoHome : { 
      borderRadius: 100,
      borderWidth : 10,
      borderColor: Theme.COLORS.GREEN,
      height: 180, 
      width: 180, 
      zIndex: 1 
    }
  });