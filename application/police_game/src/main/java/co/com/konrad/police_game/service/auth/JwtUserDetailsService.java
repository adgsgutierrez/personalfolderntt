package co.com.konrad.police_game.service.auth;

import co.com.konrad.police_game.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private Environment env;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        String username = env.getProperty(Constants.JWT_USER);
        String password = env.getProperty(Constants.JWT_PASSWORD);

        if (username.equals(s)) {
            return new User(username, password, new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User not found with username: " + s);
        }
    }
}
