﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairsPresenter
{
    
    private int index = 0;
    private int IndexTop = 55;

    public int SetPosition(int position) {
        if ( (index + position) < IndexTop ) {
            index = index + position;
        }
        return index;
    }

    public int RunIntent()
    {
        return SetPosition(RunRandom());
    }

    public int RunRandom()
    {
        return (int)Random.Range(1f, 6f);
    }

    public void SetPositionForce(int position)
    {
        index = position;
    }
}
