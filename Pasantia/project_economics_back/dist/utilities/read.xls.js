"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Xlsx = void 0;
const xlsx_1 = __importDefault(require("xlsx"));
const fs_1 = __importDefault(require("fs"));
const exceljs_1 = __importDefault(require("exceljs"));
class Xlsx {
    static read(route) {
        // return Xlsx.withXLSX(route);
        Xlsx.witXLSXInBuffer(route);
    }
    static withXLSX(route) {
        // const buf = fs.readFileSync(route);
        console.log('Iniciando lectura');
        const workSheetsFromBuffer = xlsx_1.default.readFile(route, {
            type: 'binary',
            cellFormula: true,
            cellHTML: false, cellNF: false, cellText: true,
            bookDeps: false, bookFiles: false, bookProps: false,
            bookSheets: false, raw: false, dense: false,
            WTF: false, bookVBA: false, cellDates: true, sheetStubs: false, cellStyles: false
        });
        console.log('workSheetsFromBuffer', workSheetsFromBuffer);
        return workSheetsFromBuffer;
    }
    static withExcelJs(route) {
        return new Promise((res, rej) => {
            try {
                const workbook = new exceljs_1.default.Workbook();
                const excel = fs_1.default.realpathSync(route, { encoding: 'utf8' });
                const readStream = fs_1.default.createReadStream(excel, {
                    highWaterMark: 16
                });
                const data = [];
                readStream.on('data', (chunk) => {
                    data.push(chunk);
                    // console.log('data :', chunk, chunk.length);
                });
                readStream.on('end', () => __awaiter(this, void 0, void 0, function* () {
                    // end : I am transferring in bytes by bytes called chunk
                    // Buffer.concat(data).toString()
                    yield workbook.xlsx.load(Buffer.concat(data));
                    res(workbook);
                }));
                readStream.on('error', (err) => {
                    rej(err);
                });
            }
            catch (err) {
                rej(err);
            }
        });
    }
    static witXLSXInBuffer(route) {
        const stream = fs_1.default.createReadStream(route);
        const buffers = [];
        stream.on("data", (data) => {
            buffers.push(data);
        });
        stream.on("end", () => {
            const buffer = Buffer.concat(buffers);
            const workbook = xlsx_1.default.read(buffer, {
                type: "buffer",
                WTF: true
            });
            const sheetNames = workbook.SheetNames;
            console.log('sheetNames', sheetNames);
            const worksheet = workbook.Sheets.Results;
            console.log('result', worksheet);
        });
    }
}
exports.Xlsx = Xlsx;
//# sourceMappingURL=read.xls.js.map