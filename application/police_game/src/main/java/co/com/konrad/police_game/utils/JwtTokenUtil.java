package co.com.konrad.police_game.utils;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.Serializable;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtTokenUtil implements Serializable {

    @Value("${jwt.audience}")
    String audience;
    @Value("${jwt.issuer}")
    String issuer;
    @Value("${jwt.subject}")
    String subject;
    @Value("${jwt.ttlMillis}")
    long ttlMillis;
    @Value("${jwt.secretKey}")
    String secretKey;

    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public Date getIssuedAtDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getIssuedAt);
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    private Boolean ignoreTokenExpiration(String token) {
        // here you specify tokens, for that the expiration is ignored
        return false;
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, userDetails.getUsername());
    }

    public String generateToken(String input) {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, input);
    }

    private String doGenerateToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setAudience(audience)
                .setIssuer(issuer)
                .setExpiration(new Date(System.currentTimeMillis() + ttlMillis))
                .signWith(SignatureAlgorithm.HS512, secretKey).compact();
    }

    public Boolean canTokenBeRefreshed(String token) {
        return (!isTokenExpired(token) || ignoreTokenExpiration(token));
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    public String encriptJwt(String object) {

        // The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        // We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secretKey);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
        // Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().claim("information", object).setIssuedAt(now).setAudience(audience)
                .setSubject(subject).setIssuer(issuer).signWith(signatureAlgorithm, signingKey);

        // if it has been specified, let's add the expiration
        if (ttlMillis > 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        // Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    /**
     * Decript.
     *
     * @param token the token
     * @return the string
     * @throws Exception the exception
     */
    public Object decript(String token) throws Exception {
        try {
            Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();

            claims.getIssuer().getClass().getName();
            claims.getAudience().getClass().getName();
            claims.getSubject().getClass().getName();

            if ( claims.getAudience() != null && !claims.getAudience().isEmpty() &&
                    !claims.getAudience().equals(audience) &&
                    claims.getIssuer() != null && !claims.getIssuer().isEmpty() &&
                    !claims.getIssuer().equals(issuer) &&
                    claims.getSubject() != null && !claims.getSubject().isEmpty() &&
                    !claims.getSubject().equals(subject)) {

                throw new Exception("Error to validate ");
            }
            return claims.get("information").toString().getBytes();
        } catch (Exception e) {
            throw new JwtException(e.toString());
        }
    }
}
