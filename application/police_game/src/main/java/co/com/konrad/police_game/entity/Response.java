package co.com.konrad.police_game.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@ToString
public class Response implements Serializable {

    @Setter
    @Getter
    private Integer code;

    @Setter
    @Getter
    private String message;

    @Setter
    @Getter
    private Object data;

}
