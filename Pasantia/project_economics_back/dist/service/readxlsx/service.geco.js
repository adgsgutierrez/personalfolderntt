"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceGeco = void 0;
const read_xls_1 = require("./../../utilities/read.xls");
class ServiceGeco {
    sendService(request, response) {
        const res = { result: '', sms: '' };
        try {
            const route = `${__dirname}/files/` + ServiceGeco.files[request.param("id")];
            res.sms = 'with file ' + route;
            const result = read_xls_1.Xlsx.read(route);
            console.log(result);
            res.result = 'ok';
        }
        catch (err) {
            console.error('err', err);
            res.result = 'error';
        }
        response.send(res);
    }
}
exports.ServiceGeco = ServiceGeco;
ServiceGeco.files = [
    'EXT-012426-00068 - DX- Evolución Canales Telefónica - 20210112.xlsb',
    'EXT-012426-00068 - DX- Evolución Canales Telefónica - 20201221.xlsb',
    'EXT-012426-00068 - DX- Evolución Canales Telefónica - 20200824.xlsb',
    'GECO.xlsb',
    'EXT-014647-00041 Enel App Movil 20201022.xlsb',
    'ZPS_F_ACTUAL_LINE_ITEMSSet (15).xlsx'
];
//# sourceMappingURL=service.geco.js.map