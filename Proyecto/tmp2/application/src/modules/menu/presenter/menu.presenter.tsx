import AsyncStorage from "@react-native-community/async-storage";
import { Constants } from "../../../utilities/constants";
import { Alert , BackHandler } from 'react-native';

export default class MenuPresenter {
    

    public sendTestBook(navigation): void{
        navigation.navigate('Allegory');
    }
    public sendHability(navigation): void{
        this.validateIntoView().then(
            ( isValidToInto: boolean ) => {
                if (isValidToInto){
                    navigation.navigate('ViewGame');
                } else {
                    Alert.alert('Completa la cartilla en el menú de Alegoría');
                }
            }
        )
    }
    public sendProfile(navigation): void{
        if(Constants.ISOFFLINE){
            Alert.alert('No disponible en modo Offline');
        } else {
            navigation.navigate('Profile');
        }
        
    }

    private validateIntoView(): Promise<boolean> {
        return new Promise(
            ( success , reject ) => {
                AsyncStorage.getItem(Constants.PERSISTENCE.TEST).then(
                    (data) => {
                        success( (data) ? true : false );
                    } , reject
                )
            }
        );
    }

}