import React from 'react';
import { StatusBar, Image , Text } from 'react-native';
import { Block } from 'galio-framework';
import UnityView from 'react-native-unity-view';
import HeaderApplication from '../../../components/header/header.view';

export default class ViewGame extends React.Component{



    render () {
        // @ts-expect-error
        const { navigation } = this.props;

        return (
        <Block flex>
             <HeaderApplication 
                    icon={require('./../../../../assets/images/game.png')}
                    label="Habilidad"></HeaderApplication>
                <Text>ViewGame</Text>
            <UnityView/>
        </Block>
        );
    }
}