import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import Onboarding from './../modules/onboarding/view/onboarding.view';
import Login from './../modules/login/view/login.view';
import Menu from './../modules/menu/view/menu.view';
import Allegory from '../modules/allegory/view/allegory.view';
import ViewGame from '../modules/view-game/view/viewgame.view';
import Profile from '../modules/profile/view/profile.view';
import Results from '../modules/result-allegory/view/results.view';

const Stack = createStackNavigator();

export default (props) => {
    return (
        <Stack.Navigator mode="card" headerMode="none">
            <Stack.Screen name="Onboarding" component={Onboarding} option={{ headerTransparent: true }}/>
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Menu" component={Menu} />
            <Stack.Screen name="Allegory" component={Allegory} />
            <Stack.Screen name="Allegory/Results" component={Results} />
            <Stack.Screen name="ViewGame" component={ViewGame} />
            <Stack.Screen name="Profile" component={Profile} />
        </Stack.Navigator>
        )
    };
