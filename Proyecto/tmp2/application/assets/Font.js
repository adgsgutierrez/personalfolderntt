
const montserrat_regular = require('./font/Montserrat-Regular.ttf');
const montserrat_bold = require('./font/Montserrat-Bold.ttf');

export default {
    montserrat_regular,
    montserrat_bold
}