import React from 'react';
import { Text } from 'galio-framework';
import {  Image, View } from 'react-native';
import styles from './item_menu.style';

export default class ItemMenu extends React.Component{

    constructor(props) {
        super(props);
      }

    render(){
        const children: any = this.props;
        return (
            <View onStartShouldSetResponder={ children.navigateTo } style={styles.containerButton}>
                <Image source={children.icon} style={styles.imageButton}/>
                <Text style={styles.labelButton}>{ children.label}</Text>
            </View>
        );
    }
}