import React from "react";
import { View , Text } from "react-native";
import styles from './item_result.style'

export default class ItemResult extends React.Component{

    constructor(props) {
        super(props);
    }

    private getColorHeader(): string {
        const colors = [ '#CCD626' , '#5AAB33' , '#00773A' , '#00643A'];
        var RandomNumber = Math.floor(Math.random() * colors.length);
        return colors[RandomNumber];
    }

    render(){
        const children: any = this.props;
        return (
            <View style={styles.containerParent}>
                <View style={[styles.containerTitle , {backgroundColor: this.getColorHeader()}]}>
                    <Text style={styles.textTitle}>{children.title}</Text>
                </View>
                <View style={styles.containerSubtitle}>
                    <Text style={styles.textSubTitle}>{children.description}</Text>
                </View>
            </View>
        );
    }
}