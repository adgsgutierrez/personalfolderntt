package co.com.konrad.police_game.repository;

import co.com.konrad.police_game.entity.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.Date;

@Repository
public interface TestRepository extends JpaRepository<Test, Integer> {

    @Modifying
    @Transactional
    @Query( value = "INSERT INTO test ( test_user, test_total_d, test_total_i, test_total_s, test_total_c, test_total, test_create, test_update) VALUES ( ?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)" , nativeQuery = true)
    Integer save(Integer user , Integer d , Integer i, Integer s, Integer c, Integer total, Date create, Date update);

}
