import { StyleSheet, Dimensions, Platform } from 'react-native';
import { theme } from 'galio-framework';
import Theme from './../../../../assets/Theme';

const { width, height } = Dimensions.get('screen');

const widthDimens = (width - 30);
console.log('height ' + width);
console.log('heightDimens ' + widthDimens);

export default StyleSheet.create({
  containerImage: {
    alignContent: "center",
    alignItems: "center"
  },
  containerButtons: {
    alignContent: "center",
    alignItems: "center",
    height: 320,
    marginBottom: 20,
    paddingBottom: 30
  },
  imageHeader: {
    marginTop: 30,
    marginBottom: 30,
    borderRadius: 150,
    borderColor: Theme.COLORS.GREEN,
    borderWidth: 1,
    width: 150,
    height: 150,    
  },
  containerIcon: {
    marginTop: 0,
    height: '70%',
    alignItems: 'center',
    width: width,
    zIndex: 2,
    backgroundColor: Theme.COLORS.GREEN,
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60
  },
  containerCard: {
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 15,
    marginBottom: 15,
    marginRight: 15,
    borderRadius: 15,
    borderColor: Theme.COLORS.BLACK,
    zIndex: 10,
    width: widthDimens,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    backgroundColor: Theme.COLORS.WHITE
  },
  button: {
    borderRadius: 15,
    width: '80%'
  },
  imageFooter: {
    width: 40,
    height: 40,
  },
});