import {  StyleSheet, Dimensions , Platform } from 'react-native';
import { theme } from 'galio-framework';
import Theme from './../../../../assets/Theme';

const { width , height } = Dimensions.get('screen');

const widthDimens = ( width - 30 ) ;
console.log('height ' + width);
console.log('heightDimens ' + widthDimens);

export default StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: theme.COLORS.WHITE,
      marginTop: 0,
      padding:0,
    },
    containerIcon: {
      height: '70%',
      alignItems: 'center',
      width: width,
      zIndex: 2,
      backgroundColor: Theme.COLORS.GREEN,
      borderBottomLeftRadius: 60 ,
      borderBottomRightRadius: 60
    },
    containerCard: {
      paddingTop: 0,
      paddingLeft: 20,
      paddingRight: 20,
      justifyContent: 'center',
      alignItems:'center',
      position: 'absolute',
      bottom:0,
      marginTop: '60%',
      marginLeft: 15,
      marginBottom: 15,
      marginRight: 15 ,
      borderRadius : 15,
      borderColor: Theme.COLORS.BLACK,
      zIndex:10,
      height: 350,
      width: widthDimens,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      elevation: 4,
      backgroundColor: Theme.COLORS.WHITE
    },
    textLabel: {
      textAlign: 'center',
      margin: 5,
      marginTop: 0,
      marginBottom: 20,
      color: Theme.COLORS.GREEN
    },
    inputForm: {
      marginBottom: 15,
      borderWidth: 1,
      borderColor: "#E5E5E5",
      width: '100%',
      color: Theme.COLORS.GREEN ,
      borderRadius: 10
    },
    button:{
      marginTop:10,
      borderRadius:15,
      width: '80%'
    },
    imageFooter: {
      width:40,
      height:40,
      position: 'absolute',
      bottom: 14
    },
    imageHeader: {
      marginTop:30,
      borderRadius: 150,
      borderColor: Theme.COLORS.GREEN,
      borderWidth: 5,
      width:200,
      height:200,
    }
  });