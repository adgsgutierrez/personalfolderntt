import { ServiceActual } from './../service/readxlsx/service.actual';
import { ServiceGeco } from './../service/readxlsx/service.geco';


export interface IRoutes {
    path: string;
    class: any;
    method: 'post' | 'get' | 'put' | 'patch';
}

export const Routes: IRoutes[] = [
    {
        method: 'post',
        path: '/read/geco/:id',
        class: ServiceGeco
    },{
        method: 'post',
        path: '/read/actual',
        class: ServiceActual
    }
];