import { FirebaseService } from './../firebase/firebase.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardServiceService implements CanActivate {

  constructor(private firebaseService: FirebaseService, private router: Router) { }

  canActivate(): boolean {
    if (this.firebaseService.getConfiguration()){
      return true;
    }
    this.router.navigate(['login']);
    return true;
  }
}
