﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class CanvasTest : MonoBehaviour , ICanvasTest
{
    public GameObject UiActive;
    private CanvasTestPresenter presenter = new CanvasTestPresenter();
    private TestQuestion question;
    public string category;
    public Text Label;
    public Button ButtonA;
    public Button ButtonB;
    public Button ButtonC;
    public Button ButtonD;
    // Start is called before the first frame update
    void Start()
    {
        LoadQuestions();
        // StartCoroutine(CoroutineNextScreen());
    }

    public void LoadQuestions(){
        this.presenter.SetCategorySelected(category);
        this.presenter.LoadQuestions();
        this.HideUIInterface();
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
    private IEnumerator CoroutineNextScreen()
    {
        yield return new WaitForSeconds(1f);
        this.ShowUIInterface();
    }

    public void HideUIInterface(){
        GetComponent<Canvas> ().enabled = false;
        //UiActive.GetComponent<Canvas> ().enabled = true;
     }

    public void ShowUIInterface(){
        if (this.presenter.ExistsNexQuestion()) {
            this.question = this.presenter.getQuestion();
            GetComponent<Canvas> ().enabled = true;
            //UiActive.GetComponent<Canvas> ().enabled = false;
            Label.text = this.question.Question;
            ButtonA.GetComponentsInChildren<Text>()[0].text = this.question.Options[0].Answer;
            ButtonB.GetComponentsInChildren<Text>()[0].text = this.question.Options[1].Answer;
            ButtonC.GetComponentsInChildren<Text>()[0].text = this.question.Options[2].Answer;
            ButtonD.GetComponentsInChildren<Text>()[0].text = this.question.Options[3].Answer;
        } else {
            ShowAlert("Genial" , "No cuentas con más situaciones para analizar ");
            return;
        }
    }

    public void SetButtonA(){
        this.HideUIInterface();
        if (this.question.Options[0].IsCorrect) {
            IsCorrect();
        } else {
            IsIncorrect();
        }
    }

    public void SetButtonB(){
        this.HideUIInterface();
        if (this.question.Options[1].IsCorrect) {
            IsCorrect();
        } else {
            IsIncorrect();
        }
    }

    public void SetButtonC(){
        this.HideUIInterface();
        if (this.question.Options[2].IsCorrect) {
            IsCorrect();
        } else {
            IsIncorrect();
        }
    }

    public void SetButtonD(){
        this.HideUIInterface();
        if (this.question.Options[3].IsCorrect) {
            IsCorrect();
        } else {
            IsIncorrect();
        }
    }

    private void IsCorrect(){
        ShowAlert("Genial" , "Conoces como se debe proceder");
    }

    private void IsIncorrect(){
        string sms = "";
        for (int index = 0 ; index < this.question.Options.Length ; index ++ ) {
            if (this.question.Options[index].IsCorrect) {
                sms = this.question.Options[index].Answer;
            }
        }
        ShowAlert("Ups" , "Debes mejorar dado que la respuesta correcta es " + sms);
    }

    private void ShowAlert(string title , string message){
        #if UNITY_EDITOR
             EditorUtility.DisplayDialog(title,
                message , "Cerrar");
        #endif
    }
}
