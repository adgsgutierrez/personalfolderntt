import { IResponse } from "../models/response.interface";
import { Constants } from "./constants";

export default class ServiceHttp { 

    private authentication():Promise<IResponse>{
        const header = { 'Content-Type': 'application/json' };
        const body = { user: Constants.AUTH.USR , password : Constants.AUTH.PWD};
        const host = Constants.HOST + Constants.PATH.AUTH;
        return this.serviceCallPost(host , body , header);
        
    }

    private serviceCallPost( host: string , body: any , header: any ): Promise<any>{
        const requestOptions = {
            method: 'POST',
            headers: header,
            body: JSON.stringify(body)
        };
        console.log('host ' , host);
        console.log('requestOptions ' , requestOptions);
        return fetch(host, requestOptions)
            .then(response => response.json());
    }

    public servicePost( host: string , body: any ): Promise<any>{
        return new Promise(
            ( response , reject ) =>{
                this.authentication().then(
                    (result: IResponse) => {
                        console.log('Response Aut ' + result);
                        const header = { 
                            'Content-Type': 'application/json' ,
                            'Authorization' : result.data 
                        };
                        this.serviceCallPost(host , body , header)
                        .then(response,reject)
                        .catch(reject);
                    } , reject
                );
            }
        );
    }
}