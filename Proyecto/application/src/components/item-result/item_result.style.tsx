import {  StyleSheet, Dimensions } from 'react-native';
import Theme from '../../../assets/Theme';

const { width , height } = Dimensions.get('screen');

const widthDimen = width - 40;
const widthDimenLabel = width - 150;

export default StyleSheet.create({
    containerParent: {
        marginBottom : 10,
        marginTop : 10,
        marginLeft : 20 ,
        marginRight : 20,
        borderRadius: 10,
        width : widthDimen,
        backgroundColor: Theme.COLORS.WHITE,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.23,
        elevation: 4,
    },
    containerTitle: {
      padding: 10,
      backgroundColor : Theme.COLORS.GREEN,
      borderTopLeftRadius : 10,
      borderTopRightRadius : 10,
    },
    containerSubtitle : {
        padding: 10
    },
    textTitle: {
      fontSize: 16,
      textAlign: "center",
      color: Theme.COLORS.WHITE
    },
    textSubTitle: {
      fontSize: 14,
      textAlign: "justify",
      color: Theme.COLORS.BLACK
    },
  });