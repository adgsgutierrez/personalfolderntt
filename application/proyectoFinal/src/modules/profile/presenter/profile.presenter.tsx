import { IAnswer } from "../../../models/question.interface";

export default class ProfilePresenter {

    public navigateToResults(navigation , storage): void {
        const data = storage.get('dataForResult');
        const result: IAnswer = JSON.parse(data);
        navigation.navigate('Allegory/Results' , { result });
    }

    public navigateToLogin(navigation): void {
        navigation.navigate('Login');
    }

    public saveInformation(): void {
        
    }
}