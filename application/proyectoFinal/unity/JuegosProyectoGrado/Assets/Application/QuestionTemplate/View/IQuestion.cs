﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IQuestion 
{
   void NextQuestion();
   void ShowFinishGame();
   void ShowPointsGame();
}
