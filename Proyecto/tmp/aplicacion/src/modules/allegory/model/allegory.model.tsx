import { IQuestion } from "../../../models/question.interface";

export default class AllegoryModel {

    public loadQuestions(): IQuestion[] {
        return require('./../../../../assets/src/Questions.json');
    } 

    
}