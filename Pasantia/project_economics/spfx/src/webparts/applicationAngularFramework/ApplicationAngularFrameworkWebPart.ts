import "reflect-metadata";
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SPComponentLoader } from '@microsoft/sp-loader'; // For load JS and CSS cdn.
import { sp } from '@pnp/sp';
require('zone.js');
import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import * as strings from 'ApplicationAngularFrameworkWebPartStrings';

export interface IApplicationAngularFrameworkWebPartProps {
  description: string;
}

export default class ApplicationAngularFrameworkWebPart extends BaseClientSideWebPart<IApplicationAngularFrameworkWebPartProps> {

  public constructor() {
    super();
    SPComponentLoader.loadCss('./app/styles.8ad3432f3a3c7c795045.css');
    // SPComponentLoader.loadScript('https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js');
    // SPComponentLoader.loadScript('https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js');
    // SPComponentLoader.loadCss('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css');

    SPComponentLoader.loadScript('./app/runtime.fcef4b1515e862c29e4c.js');
    SPComponentLoader.loadScript('./app/polyfills.9f8e04a03cfa32bd8bf9.js');
    SPComponentLoader.loadScript('./app/main.717ff2dbddf6cd37e682.js');
  }

  public render(): void {
    window['webPartContext'] = this.context;
    sp.setup({
          spfxContext: this.context
    });
    this.domElement.innerHTML = '<iframe class="embed-responsive-item" src="./app/index.html"></iframe>';
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
