package com.application;

import android.content.Intent;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;


public class BridgeModule extends ReactContextBaseJavaModule {

    private ReactApplicationContext context = getReactApplicationContext();

    public BridgeModule(@Nullable ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @NonNull
    @Override
    public String getName() {
        return "GameModule";
    }

    @ReactMethod
    public void NavigateToGame(String flag){

        Intent intent = new Intent(context , UnityPlayerActivity.class);
        if(intent.resolveActivity(context.getPackageManager()) != null ){
            new android.os.Handler(Looper.getMainLooper()).postDelayed(
            new Runnable() {
                public void run() {
                    Log.i("tag", "This'll run 300 milliseconds later");
                    UnityPlayer.UnitySendMessage("Background" , "handleMessage" , flag);
                }
            },
            500);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }

    }
}
