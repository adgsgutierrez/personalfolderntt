package co.com.konrad.police_game.service.auth;

import co.com.konrad.police_game.entity.Auth;
import co.com.konrad.police_game.entity.Response;
import co.com.konrad.police_game.utils.Constants;
import co.com.konrad.police_game.utils.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@Qualifier("AuthServiceImpl")
@Slf4j
public class AuthServiceImpl implements IService {

    @Autowired
    private Response response;

    @Autowired
    private Environment env;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Value("${jwt.user}")
    private String userAuth;
    @Value("${jwt.password}")
    private String passwordAuth;


    @Autowired
    private UserDetailsService jwtInMemoryUserDetailsService;

    @Override
    public Response getAuthorization(Auth input) throws Exception {
        boolean validation = (input.getUser().equals(userAuth) && input.getPassword().equals(passwordAuth));

        response.setCode( (validation)? Constants.SUCCESS_CODE : Constants.FAIL_AUTH_CODE );
        response.setMessage( (validation)? Constants.SUCCESS_SMS : Constants.FAIL_AUTH_SMS );
        response.setData(jwtTokenUtil.encriptJwt(input.toString()));
        return response;
    }

    private void authenticate(String username, String password) throws Exception {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
