﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sphere : MonoBehaviour
{
    private AudioSource audioData;
    public Text textLanzamientos;
    public Text textPuntos;
    private Vector3 positionInitial;
    public ParticleSystem explosion;
    private ShootPresenter presenter = new ShootPresenter();
    private GameObject Question;

    // Start is called before the first frame update
    void Start()
    {
        positionInitial = transform.position;
        Question = GameObject.Find("Question");
        ICanvasTest active = Question.GetComponent( typeof(ICanvasTest) ) as ICanvasTest;
        active.LoadQuestions();
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Do something " + collision.gameObject.tag );
        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (collision.gameObject.name == "Goal")
        {
            audioData = collision.gameObject.GetComponent<AudioSource>();
            audioData.Play(0);
            if (collision.contacts.Length > 0) {
                ContactPoint contact = collision.contacts[0];
                StartCoroutine("ActivateParticleSystem");
                Debug.Log( " - Y:" + contact.point.y );
                presenter.GetPosition(contact.point.y);
                IsReset();
                SendQuestion();
            }
        }

        //Check for a match with the specific tag on any GameObject that collides with your GameObject
        if (collision.gameObject.tag == "Finish")
        {
            IsReset();
        }
    }

    public IEnumerator ActivateParticleSystem()
    {
        explosion.transform.position = transform.position;
        explosion.Play();
        yield return new WaitForSeconds(.3f);
        explosion.Stop();
    }

    private void IsReset()
    {
        if (presenter.IsNext()) {
            transform.position = positionInitial;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
        SetData();
    }

    private void SetData(){
        textLanzamientos.text = presenter.GetLanzamientos() + " Lanzamientos "  ;
        textPuntos.text =  "Puntos: " + presenter.GetPuntos();
    }

    private void SendQuestion(){
        ICanvasTest active = Question.GetComponent( typeof(ICanvasTest) ) as ICanvasTest;
        active.ShowUIInterface();
    }
}
