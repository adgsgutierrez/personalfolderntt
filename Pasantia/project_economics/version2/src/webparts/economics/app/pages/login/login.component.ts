import { IConfiguration } from './../../interface/config.interface';
import { environment } from './../../../environments/environment.prod';
import { Component, OnInit } from '@angular/core';
import { Utils } from 'src/app/utilities/functions';
import { UserService } from 'src/app/service/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public code: string;
  public isValidNumber: boolean;
  public isError: boolean;

  constructor( private userService: UserService , private router: Router) { }

  ngOnInit() {
    this.isValidNumber = false;
    this.isError = false;
  }

  public sendToDashboard(): void {
    this.userService.searchCode(this.code).then(
      (user) => {
        Utils.setDataInSessionStorage(user);
        this.userService.searchMenuUser().then(
          (data: IConfiguration) => {
            if (data.status && data.access.length > 0 ) {
              this.router.navigate([data.access[0].value]);
            } else {
              this.router.navigate([environment.menu.error]);
            }
          }
        );
      } , () => {
        this.isError = true;
      }
    );
  }

  public isValidNumberFunction(): void {
    this.isValidNumber = Utils.validNumber(this.code);
  }

}
