package co.com.konrad.police_game.service.test;

import co.com.konrad.police_game.entity.Response;
import co.com.konrad.police_game.entity.Test;
import co.com.konrad.police_game.repository.TestRepository;
import co.com.konrad.police_game.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@Qualifier("TestServiceImpl")
public class TestServiceImpl implements  IService {

    @Autowired
    private Response response;

    @Autowired
    private TestRepository testRepository;

    @Override
    public Response save(Test test) {
        try {
            Integer result = this.testRepository.save(
                    test.getTest_user(),
                    test.getTest_total_d(), test.getTest_total_i(), test.getTest_total_s(), test.getTest_total_c(), test.getTest_total(),
                    test.getTest_create(), test.getTest_update()
            );
            log.info("Result Query " + result.toString());
            response.setCode((result == 1) ? Constants.SUCCESS_CODE : Constants.ERNO_CODE);
            response.setMessage((result == 1) ? Constants.SUCCESS_SMS : Constants.ERNO_SMS);
            response.setData(null);
        } catch (Exception e) {
            response.setData(null);
            response.setCode( Constants.ERNO_CODE );
            response.setMessage( Constants.ERNO_SMS );
        }
        return response;
    }
}
