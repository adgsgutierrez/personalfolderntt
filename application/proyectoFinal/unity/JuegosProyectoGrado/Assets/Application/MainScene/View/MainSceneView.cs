﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSceneView : MonoBehaviour , IMainScene
{
    MainScenePresenter presenter;
    string javaMessage = "";
    // Start is called before the first frame update
    void Start()
    {
        this.presenter = new MainScenePresenter(this);
    }

    void handleMessage(string message) {
		Debug.Log("onMessage:" + message);
        javaMessage = message;
	}

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NextViewRandom()
    {
        StartCoroutine(CoroutineNextScreen());
    }

    private IEnumerator CoroutineNextScreen()
    {
        yield return new WaitForSeconds(1f);
        int nextScene = 0 ;
        switch (javaMessage)
        {   
            case "1":
                nextScene = 1;
            break;
            case "2":
                nextScene = 2;
            break;
            case "3":
                nextScene = 3;
            break;
            default:
                nextScene = 1;
            break;
        }
        Debug.Log("Scene next scene:" + nextScene);
        Debug.Log("JavaMessage scene:" + javaMessage);
        UnityEngine.SceneManagement.SceneManager.LoadScene(nextScene);
    }
}
