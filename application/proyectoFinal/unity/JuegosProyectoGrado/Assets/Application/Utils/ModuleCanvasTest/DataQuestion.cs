
 using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;
 using System;
 using System.IO;

[Serializable]
public class DataQuestion
{
    public List<TestQuestion> Social;
    public List<TestQuestion> SocioCultural;
    public List<TestQuestion> Servicio;
}