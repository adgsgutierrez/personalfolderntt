var path = require('path');
plugins: [
  new webpack.ContextReplacementPlugin(/\@angular(\\|\/)core(\\|\/)esm5/, path.join(__dirname, './src')),
  new HtmlWebpackPlugin({
    template: './src/index.html',
    filename: 'index.html',
    inject: 'body'
  }),
  new webpack.DefinePlugin({
    // global app config object
    config: JSON.stringify({
      apiUrl: 'http://localhost:4000'
    })
  })
]
