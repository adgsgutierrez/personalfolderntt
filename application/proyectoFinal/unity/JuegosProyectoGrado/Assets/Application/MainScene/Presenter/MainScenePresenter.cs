﻿ using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;
 using UnityEditor;
 using Newtonsoft.Json;

public class MainScenePresenter
{
    private Profile profile { get; }
    public TextAsset jsonFile;
    
    public MainScenePresenter(IMainScene scene)
    {
        var jsonTextFile = Resources.Load<TextAsset>(Constants.FILE_TMP);
        this.profile = JsonConvert.DeserializeObject<Profile>(jsonTextFile.text);
        SetPrefProfile();
        scene.NextViewRandom();
    }

    private void SetPrefProfile()
    {
        PlayerPrefs.SetString(Constants.PROFILE_FOR_USE , JsonConvert.SerializeObject(this.profile) );
    }
}
