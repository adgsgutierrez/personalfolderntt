import { IUser } from './../../interface/user.interface';
import { UserService } from './../../service/user/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit {

  public users: IUser[];
  public filter: string;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.filter = '';
    this.filterData();
  }

  public filterData(): void {
    this.userService.getAllUsers().subscribe(
      (users: IUser[]) => {
        if (this.filter && this.filter.trim() !== '') {
          this.users = users.filter((item) => {
            return (item.name.toLowerCase().indexOf(this.filter.toLowerCase()) > -1);
          });
        } else {
          this.users = users;
        }
      }
    );
  }

  public update(user: IUser): void {

  }
}
