package co.com.konrad.police_game.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@ToString
@Table(name = "test")
public class Test implements Serializable {

    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="test_id")
    private Integer test_id;

    @Getter
    @Setter
    @Column(name="test_user")
    private Integer test_user;

    @Getter
    @Setter
    @Column(name="test_total_d")
    private Integer test_total_d;

    @Getter
    @Setter
    @Column(name="test_total_i")
    private Integer test_total_i;

    @Getter
    @Setter
    @Column(name="test_total_s")
    private Integer test_total_s;

    @Getter
    @Setter
    @Column(name="test_total_c")
    private Integer test_total_c;

    @Getter
    @Setter
    @Column(name="test_total")
    private Integer test_total;

    @Getter
    @Setter
    @Column(name="test_create")
    private Date test_create;

    @Getter
    @Setter
    @Column(name="test_update")
    private Date test_update;

}
