import { FirebaseService } from './../../service/firebase/firebase.service';
import { ProfilesComponent } from './../profiles/profiles.component';
import { TlsComponent } from './../tls/tls.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { MenuComponent } from '../../components/menu/menu.component';


@NgModule({
  declarations: [
    MenuComponent,
    DashboardComponent,
    TlsComponent,
    ProfilesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DashboardRoutingModule
  ],
  providers:[FirebaseService],
  entryComponents: [MenuComponent]
})
export class DashboardModule { }
