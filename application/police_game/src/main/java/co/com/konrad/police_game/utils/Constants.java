package co.com.konrad.police_game.utils;

public class Constants {

    public final static Integer SUCCESS_CODE = 200;
    public final static Integer ERNO_CODE = 500;
    public final static Integer NO_DATA_CODE = 400;
    public final static Integer FAIL_AUTH_CODE = 403;

    public final static String SUCCESS_SMS = "OK";
    public final static String ERNO_SMS = "Erno generic";
    public final static String NO_DATA_SMS = "No data";
    public final static String FAIL_AUTH_SMS = "Fail Authentication";

    public final static String JWT_USER = "jwt.user";
    public final static String JWT_PASSWORD = "jwt.password";

    public final static String HEADER_PETITION = "Authorization";

    public final static String QUERY_SEARCH_USER = "from User where user_user = :user and user_password= :pwd";
}
