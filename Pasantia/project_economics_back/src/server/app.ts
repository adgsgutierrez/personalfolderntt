import express from 'express';
import { Routes } from './routes';

export class App{

    private app: any;
    private static port: number = 3000;

    public start(): void {
        this.app = express();
        this.loadRoutes();
    }

    private loadRoutes(): void {
        for(const route of Routes){
            console.log(`Create route ${route.path} form method ${route.method}`);
            const method = new route.class();
            this.app.route(route.path)[route.method](
                (req: any,res: any) =>{
                    method.sendService(req,res);
                }
            );
        }
        this.listenPort();
    }

    private listenPort(): void{
        this.app.listen(
            App.port , (err: any) => {
                if (err) {
                  return console.error(err);
                }
                return console.log(`server is listening on ${App.port}`);
              }
        );
    }
}