import { IResponse } from "../../../models/response.interface";
import { Constants } from "../../../utilities/constants";
import LoginModel from "../model/login.model";
import { Alert } from 'react-native';
import AsyncStorage from "@react-native-community/async-storage";

export default class LoginPresenter {

    private loginModel: LoginModel = new LoginModel();

    public sendToLogin(navigation ,  state ): void{
        const { userName } = state;
        const { userPassword }  = state ;
        this.loginModel.LoginApp(userName , userPassword).then(
            ( result: IResponse ) => {
                if (result.code == Constants.STATUS.SUCCESS){
                    AsyncStorage.setItem(Constants.PERSISTENCE.USER , result.data);
                    this.navigateToMenu(navigation);
                } else {
                    Alert.alert('Error de Usuario y/o Clave');
                }
            } , ( reject ) => {
                Alert.alert('Error al momento de consultar el usuario');
            }
        )
    }

    public validateTologin(): Promise<boolean>{
        return new Promise (
            (result) => {
                this.loginModel.SearchLocalData().then(
                    (data) => {
                        result( (data) ? true : false );
                    }
                );
            }
        );
    }


    public navigateToMenu(navigation): void {
        navigation.replace('Menu');
    }
}