import React from 'react';
import { Image, View } from 'react-native';
import SyncStorage from 'sync-storage';
import { Block, Input, Button } from 'galio-framework';
import HeaderApplication from '../../../components/header/header.view';
import styles from './profile.style';
import { Images } from '../../../../assets/Images';
import ItemMenu from '../../../components/item-menu/item_menu.view';
import { ScrollView } from 'react-native-gesture-handler';
import ProfilePresenter from '../presenter/profile.presenter';

export default class Profile extends React.Component {

    private presenter: ProfilePresenter = new ProfilePresenter();

    render() {
        // @ts-expect-error
        const { navigation } = this.props;

        return (
            <View>
                <HeaderApplication
                    icon={require('./../../../../assets/images/profile.png')}
                    label="Perfil"></HeaderApplication>
                <ScrollView>
                    <View style={styles.containerImage}>
                        <Image style={styles.imageHeader} source={{uri: 'https://www.animalpolitico.com/wp-content/uploads/2012/09/naranjo001.jpg'}} />
                    </View>
                    <Block style={styles.containerCard}>
                        <Input placeholder="Nombre" />
                        <Input placeholder="Apellido" />
                        <Input placeholder="Rango" />
                        <Button style={styles.button} onPress={() => { this.presenter.saveInformation() }}>Guardar</Button>
                    </Block>
                    <View style={styles.containerButtons}>
                        <ItemMenu 
                            navigateTo={ () => { this.presenter.navigateToResults(navigation , SyncStorage) } } 
                            icon={require('./../../../../assets/images/alegoria.png')}
                            label="Ver Resultados">
                        </ItemMenu>
                        <ItemMenu
                            navigateTo={ () => { this.presenter.navigateToLogin(navigation) } } 
                            icon={require('./../../../../assets/images/cerrar_session.png')}
                            label="Cerrar Sesión">
                        </ItemMenu>
                        <Image source={ Images.imageLogo } style={styles.imageFooter}/>
                    </View>
                </ScrollView>
            </View>
        );
    }
}