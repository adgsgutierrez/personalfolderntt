
import React from 'react';
import { StatusBar, Image } from 'react-native';
import { Block } from 'galio-framework';
import { Images } from './../../../../assets/Images';
import styles from './onboarding.style';
import OnboardingPresenter from '../presenter/onboarding.presenter';

export default class OnboardingView extends React.Component {

    private onboardingPresenter = new OnboardingPresenter();

    componentDidMount() {
        setTimeout(
            () => {
                const { navigation } = this.props;
                this.onboardingPresenter.sendToLogin( navigation );
            },
            2000
        );
    }

    render() {
        
        return (
            <Block flex style={styles.container}>
                <StatusBar barStyle="light-content" />
                <Image source={Images.imageApp} style={styles.imageLogoHome} />
            </Block>
        );
    }
}