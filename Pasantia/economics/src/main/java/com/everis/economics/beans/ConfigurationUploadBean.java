package com.everis.economics.beans;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@ComponentScan(basePackageClasses = UploadFileResponse.class)

public class ConfigurationUploadBean {

    @Bean
    public UploadFileResponse getResponse() {
        return new UploadFileResponse();
    }
}
