package co.com.konrad.police_game.configuration;

import co.com.konrad.police_game.entity.Response;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@ComponentScan(basePackageClasses = Response.class)
public class BeanResponse {

    @Bean
    public Response getResponse() {
        return new Response();
    }
}
