import { environment } from './../../../environments/environment';
import { Router } from '@angular/router';
import { IConfiguration } from './../../interface/config.interface';
import { Component, OnInit } from '@angular/core';
import { UserService } from './../../service/user/user.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public configuration: IConfiguration;

  constructor(private userService: UserService , private route: Router) {
    this.configuration = {
      access : [],
      status : false
    }
  }

  ngOnInit() {
    this.userService.searchMenuUser().then(
      (data: IConfiguration) => {
        if (data.status) {
          this.configuration = data;
        } else {
          this.route.navigate([environment.menu.error]);
        }
      }
    );
  }

  public logout(): void{
    localStorage.clear();
    sessionStorage.clear();
    this.route.ngOnDestroy();
    this.route.navigateByUrl('/' + environment.menu.login);
  }

}
