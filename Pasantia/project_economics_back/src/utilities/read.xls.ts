import XLSX from 'xlsx';
import fs from 'fs';
import Excel from 'exceljs';
import { Stream } from 'stream';


export class Xlsx {

    public static read ( route: string ){
        // return Xlsx.withXLSX(route);
        Xlsx.witXLSXInBuffer(route);
    }


    private static  withXLSX(route:string){
        // const buf = fs.readFileSync(route);
        console.log('Iniciando lectura');
        const workSheetsFromBuffer = XLSX.readFile(route,
            {
                type:'binary',
                cellFormula : true,
                cellHTML : false, cellNF : false, cellText : true,
                bookDeps : false, bookFiles : false, bookProps : false,
                bookSheets : false, raw : false, dense : false,
                WTF : false, bookVBA : false, cellDates : true, sheetStubs : false, cellStyles : false
            }
        );
        console.log('workSheetsFromBuffer' , workSheetsFromBuffer);
        return workSheetsFromBuffer;
    }

    private static withExcelJs(route:string){
        return new Promise (
            (res , rej) => {
                try{
                    const workbook = new Excel.Workbook();
                    const excel = fs.realpathSync(route,{encoding:'utf8'});
                    const readStream = fs.createReadStream(excel, {
                        highWaterMark: 16
                    });
                    const data:any = [];
                    readStream.on('data', (chunk) => {
                        data.push(chunk);
                        // console.log('data :', chunk, chunk.length);
                    });
                    readStream.on('end', async () => {
                        // end : I am transferring in bytes by bytes called chunk
                        // Buffer.concat(data).toString()
                        await workbook.xlsx.load(Buffer.concat(data));
                        res(workbook);
                    })
                    readStream.on('error', (err) => {
                        rej(err);
                    })
                }catch(err){
                    rej(err);
                }
            }
        );
    }


    private static witXLSXInBuffer(route: string){
        const stream = fs.createReadStream( route );
        const buffers: any[] = [];
        stream.on("data", (data) => {
            buffers.push(data);
        });

        stream.on("end",() => {

            const buffer = Buffer.concat(buffers);
            const workbook = XLSX.read(buffer, {
                type: "buffer",
                WTF: true
            });

            const sheetNames = workbook.SheetNames;
            console.log('sheetNames' , sheetNames);
            const worksheet = workbook.Sheets.Results;
            console.log('result' , worksheet);
        });
    }
}