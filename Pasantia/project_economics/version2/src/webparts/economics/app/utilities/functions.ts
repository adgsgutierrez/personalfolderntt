import { IUser } from '../interface/user.interface';
import { CryptoTls } from './crypto';
import { SESSION_STORAGE } from './constantes';

export class Utils {

  public static validNumber(characters: string): boolean {
    let pattern = /^\d+$/;
    return pattern.test(characters);
  }

  public static setDataInSessionStorage( user: IUser ): void {
    const userCrypto = CryptoTls.encode(user);
    sessionStorage.setItem(SESSION_STORAGE.USER , userCrypto);
  }

  public static getDataInSessionStorage(): IUser {
    const objectUser = CryptoTls.decode( sessionStorage.getItem(SESSION_STORAGE.USER ) );
    return objectUser;
  }
}
