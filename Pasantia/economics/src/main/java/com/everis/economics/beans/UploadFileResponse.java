package com.everis.economics.beans;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UploadFileResponse {

    @Getter
    @Setter
    private String fileName;
    @Getter
    @Setter
    private String fileDownloadUri;
    @Getter
    @Setter
    private String fileType;
    @Getter
    @Setter
    private long size;

}
