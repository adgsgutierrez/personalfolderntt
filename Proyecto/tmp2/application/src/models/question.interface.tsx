export interface IQuestion {
    option_number : number;
    options: Option[];
}

export interface Option {
    name : string;
    category : string;
}

export interface IITemAnswer{
    mas : number;
    menos: number;
}

export interface IAnswer {
    D : IITemAnswer;
    I : IITemAnswer;
    S : IITemAnswer;
    C : IITemAnswer;
}

export interface IQuestionView {
    title : string;
    question : IQuestion;
}

export interface IDetailAnswerSelected {
    title : string;
    description: string;
}

export interface IAnswerSelected {
    title: string;
    features: IDetailAnswerSelected[];
}