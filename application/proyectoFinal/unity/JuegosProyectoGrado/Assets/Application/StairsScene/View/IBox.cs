﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBox
{
   Vector3 GetPositionCenter();

   bool IsFail();

    bool IsPortal();
   
}
