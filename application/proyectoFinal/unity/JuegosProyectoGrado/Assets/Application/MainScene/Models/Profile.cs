﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Profile 
{
    public string name { get ; set ; }
    public Questions[] questions { get ; set ; }
}
