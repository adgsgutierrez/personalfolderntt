import React from "react";
import { Block, Text } from 'galio-framework';
import style from './allegory.style';
import AllegoryPresenter from "../presenter/allegory.presenter";
import HeaderApplication from "../../../components/header/header.view";
import { View } from "react-native";
import SyncStorage from 'sync-storage';
import { IAnswer, IQuestionView } from "../../../models/question.interface";
import { IAllegoryView } from "./iallegory.view";

export default class Allegory extends React.Component implements IAllegoryView{

    private allegoryPresenter = new AllegoryPresenter(this);

    constructor(prop){
        super(prop);
        this.allegoryPresenter.loadInformation();
        let question: IQuestionView = this.allegoryPresenter.nextQuestion();
        this.state = {
            title : question.title ,
            questionA : question.question.options[0],
            questionB : question.question.options[1],
            questionC : question.question.options[2],
            questionD : question.question.options[3],
        }
    }

    viewResults(result: IAnswer): void {
        // @ts-expect-error
        const { navigation } = this.props;
        SyncStorage.set('dataForResult', JSON.stringify(result));
        navigation.navigate('Allegory/Results' , { result });
    }

    refreshData() {
        let question: IQuestionView = this.allegoryPresenter.nextQuestion();
        this.setState(
            {
                title : question.title ,
                questionA : question.question.options[0],
                questionB : question.question.options[1],
                questionC : question.question.options[2],
                questionD : question.question.options[3],
            }
        );
    }

    render () {
       
        return (
            <Block>
                <HeaderApplication 
                    icon={require('./../../../../assets/images/alegoria.png')}
                    label="Alegoría"></HeaderApplication>
                <Text style={style.textTitle}>¿Cómo Eres?</Text>
                <Text style={style.textSubTitle}>{ this.state.title }</Text>
                <View style={style.containerButtons}>
                    <View style={style.containerColumButtons}>
                        <View style={[style.button , { backgroundColor: this.allegoryPresenter.getNewColor(true)}]} 
                        onStartShouldSetResponder={ () => {this.allegoryPresenter.saveThisAnswer( this.state.questionA ) } }>
                            <Text style={style.textButton}>{ this.state.questionA.name }</Text>
                        </View>
                        <View style={[style.button , { backgroundColor: this.allegoryPresenter.getNewColor(false)}]} 
                        onStartShouldSetResponder={ () => {this.allegoryPresenter.saveThisAnswer( this.state.questionB ) }}>
                            <Text style={style.textButton}>{ this.state.questionB.name }</Text>
                        </View>
                    </View>
                    <View style={style.containerColumButtons}>
                        <View style={[style.button , { backgroundColor: this.allegoryPresenter.getNewColor(false)}]}
                        onStartShouldSetResponder={ () => {this.allegoryPresenter.saveThisAnswer( this.state.questionC ) }}>
                            <Text style={style.textButton}>{ this.state.questionC.name }</Text>
                        </View>
                        <View style={[style.button , { backgroundColor: this.allegoryPresenter.getNewColor(false)}]}
                        onStartShouldSetResponder={ () => {this.allegoryPresenter.saveThisAnswer( this.state.questionD ) }}>
                            <Text style={style.textButton}>{ this.state.questionD.name }</Text>
                        </View>
                    </View>
                </View>
            </Block>
        );
    }
}