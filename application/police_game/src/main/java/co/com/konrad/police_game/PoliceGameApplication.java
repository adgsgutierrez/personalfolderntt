package co.com.konrad.police_game;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoliceGameApplication {

	public static void main(String[] args) {
		SpringApplication.run(PoliceGameApplication.class, args);
	}

}
