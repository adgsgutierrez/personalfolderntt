export interface IItemManu {
  title: string;
  value: string;
}

export interface IConfiguration {
  access: IItemManu[];
  status: boolean;
}
