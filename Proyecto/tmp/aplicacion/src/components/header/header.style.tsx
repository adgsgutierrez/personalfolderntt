import {  StyleSheet, Dimensions } from 'react-native';
import Theme from '../../../assets/Theme';

const { width , height } = Dimensions.get('screen');

const widthDimen = width;
const widthDimenLabel = width - 150;

export default StyleSheet.create({
    containerheader: {
      height: 70,
      paddingLeft:20,
      borderRadius: 0,
      marginBottom: 0,
      width: widthDimen,
//   justifyContent: 'center', 
      alignItems: 'center',
      flexDirection: "row",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      elevation: 4,
      backgroundColor: Theme.COLORS.WHITE
      
    },
    imageHeader: {
      height: 60,
      width: 60
    },
    labelButton: {
        width :widthDimenLabel,
        textAlign: "center",
        color: Theme.COLORS.GREEN,
        fontSize: 20
    }
  });