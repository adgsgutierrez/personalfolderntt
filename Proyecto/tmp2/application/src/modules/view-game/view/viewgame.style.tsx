import {  StyleSheet, Dimensions } from 'react-native';
import Theme from '../../../../assets/Theme';

const { width , height } = Dimensions.get('screen');

export default StyleSheet.create({
    containerBanner: {
      height: '100%',
      width: '100%'
    },
    imageBanner: {
      height: 280,
      width: '100%',
      resizeMode: "cover"
    },
    containerMenu: {
      position: "absolute",
      zIndex:3,
      padding:20,
      height: ( height - 320 ),
      width,
      marginTop: 260,
      bottom: 0,
      left:0,
      right:0,
      backgroundColor:Theme.COLORS.WHITE,
      borderTopStartRadius: 30,
      borderTopEndRadius: 30,
     alignItems: "center" 
    },
    imageFooter: {
      width:40,
      height:40,
    },
  });