const imageBanner = require('./images/banner.png');
const imageLogo = require('./images/logo_police.png');
const imageApp = require('./images/logo_app.png');

const imageBook = require('./images/alegoria.png');
const imageGame = require('./images/game.png');
const imageProfile = require('./images/profile.png');


export const Images = {
    imageBanner,
    imageLogo,
    imageApp,
    imageBook,
    imageGame,
    imageProfile,
}

export default [
    imageBanner,
    imageLogo,
    imageApp,
    imageBook,
    imageGame,
    imageProfile
];