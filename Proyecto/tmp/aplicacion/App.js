import { Block, GalioProvider } from 'galio-framework';
import { NavigationContainer } from '@react-navigation/native';

import React from 'react';
import { Image } from 'react-native';
import Screens from './src/configuration/stack.screen';
import Images  from './assets/Images';
import Font from './assets/Font';
import Theme from './assets/Theme';

const App: () => React$Node = () => {
  return (
    <NavigationContainer>
      <GalioProvider theme={Theme}>
        <Block flex>
          <Screens />
        </Block>
      </GalioProvider>
    </NavigationContainer>
  );
};


export default App;
