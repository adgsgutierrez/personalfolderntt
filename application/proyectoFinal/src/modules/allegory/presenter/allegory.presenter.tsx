import { IQuestion , IQuestionView , IAnswer , IITemAnswer, Option } from "../../../models/question.interface";
import { Functions } from "../../../utilities/functions";
import AllegoryModel from "../model/allegory.model";
import { IAllegoryView } from "../view/iallegory.view";

export default class AllegoryPresenter {

    private allegoryView: IAllegoryView;
    private allegoryModel = new AllegoryModel();
    private flag : number;
    private questions : IQuestion[];
    private answers: IAnswer = {
        C : { mas : 0 , menos : 0 },
        D: { mas : 0 , menos : 0 },
        I: { mas : 0 , menos : 0 },
        S: { mas : 0 , menos : 0 }
    };

    constructor(view: IAllegoryView){
        this.allegoryView = view;
    }

    private isMore: boolean = true;
    private colors: string[] = [ '#CCD626' , '#5AAB33' , '#00773A' , '#00643A'];
    private colorTmp: string[] = [];
    
    public loadInformation(): void {
        this.flag = 0;
        this.questions = Functions.orderRandom( this.allegoryModel.loadQuestions() , 'option_number');
    } 

    public isNextQuestion() : boolean {
        return ( (this.flag + 1 ) < this.questions.length );
    }

    public nextQuestion(): IQuestionView {
        let question : IQuestionView = {
            title: (this.isMore)? 'Selecciona si eres más ...' : 'Selecciona si eres menos ...',
            question : this.questions[this.flag]
        }
        return question;
    }

    public saveThisAnswer(answer: Option): void {
        if (this.isNextQuestion()) {
            this.answers[answer.category].mas += (this.isMore) ? 1 : 0 ;
            this.answers[answer.category].menos += (!this.isMore) ? 1 : 0 ;
            if (!this.isMore ) { this.flag = this.flag + 1 }
            this.isMore = !this.isMore;
            this.allegoryView.refreshData();
        } else {
            this.allegoryView.viewResults(this.answers);
        }
    }

    public getNewColor(index: boolean): string {
        if (index) {
            for (let data of this.colors) {
                this.colorTmp.push(data);
            }
        }
        var RandomNumber = Math.floor(Math.random() * this.colorTmp.length);
        var color = this.colorTmp[RandomNumber];
        this.colorTmp.splice(RandomNumber, 1);
        return color;
    }
}