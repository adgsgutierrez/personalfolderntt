// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCFwS0J589LF1MN2szA-21M9h2K1eQ9wy4',
    authDomain: 'demotl-s.firebaseapp.com',
    databaseURL: 'https://demotl-s.firebaseio.com',
    projectId: 'demotl-s',
    storageBucket: 'demotl-s.appspot.com',
    messagingSenderId: '659679152070',
    appId: '1:659679152070:web:69fb948c351830dcc954c7'
  },
  session : {
    data : 'BJKSLORP'
  },
  menu: {
    login : 'login',
    error: 'error',
    home : 'table-control/home',
    tls : 'table-control/tls',
    profile: 'table-control/profiles'
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
