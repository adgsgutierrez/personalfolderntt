import { ProfilesComponent } from './../profiles/profiles.component';
import { TlsComponent } from './../tls/tls.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
  { path: 'home', component: DashboardComponent },
  { path: 'tls', component: TlsComponent },
  { path: 'profiles', component: ProfilesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
