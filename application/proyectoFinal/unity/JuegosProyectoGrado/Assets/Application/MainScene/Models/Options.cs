﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Options
{
    public int index { get ; set ; }
    public string text { get ; set ; }
}
