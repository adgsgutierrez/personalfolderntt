﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootPresenter
{
    private int points;

    private int lanzamientos;

    public ShootPresenter()
    {
        points = 0;
        lanzamientos = 10 ;
    }

    public void GetPosition(float altura)
    {
        lanzamientos -= 1;
        if (altura < -70f) 
        {
            points += 300;
        }
        else if ( altura < -60.0f && altura > -69.0f )
        {
            points += 200;
        }
        else if ( altura < -50.0f && altura > -59.0f )
        {
            points += 100;
        }
        else
        {
            points += 50;
        }
        Debug.Log("Points " + points);
    }

    public bool IsNext()
    {
        return (lanzamientos > 0);
    }

    public int GetLanzamientos()
    {
        return lanzamientos;
    }

    public int GetPuntos()
    {
        return points;
    }
}
