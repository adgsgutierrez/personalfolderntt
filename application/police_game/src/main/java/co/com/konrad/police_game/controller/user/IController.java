package co.com.konrad.police_game.controller.user;

import co.com.konrad.police_game.entity.Response;
import co.com.konrad.police_game.entity.User;

public interface IController {

    Response search(User user);
}
