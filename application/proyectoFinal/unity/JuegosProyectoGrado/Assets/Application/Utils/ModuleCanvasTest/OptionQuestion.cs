
 using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;
 using System;
 using System.IO;

[Serializable]
public class OptionQuestion
{
    public string Answer;
    public bool IsCorrect;
}