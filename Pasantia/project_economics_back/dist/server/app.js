"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
const express_1 = __importDefault(require("express"));
const routes_1 = require("./routes");
class App {
    start() {
        this.app = express_1.default();
        this.loadRoutes();
    }
    loadRoutes() {
        for (const route of routes_1.Routes) {
            console.log(`Create route ${route.path} form method ${route.method}`);
            const method = new route.class();
            this.app.route(route.path)[route.method]((req, res) => {
                method.sendService(req, res);
            });
        }
        this.listenPort();
    }
    listenPort() {
        this.app.listen(App.port, (err) => {
            if (err) {
                return console.error(err);
            }
            return console.log(`server is listening on ${App.port}`);
        });
    }
}
exports.App = App;
App.port = 3000;
//# sourceMappingURL=app.js.map