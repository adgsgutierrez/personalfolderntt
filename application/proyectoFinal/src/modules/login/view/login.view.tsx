import React from 'react';
import { StatusBar, Image , Dimensions } from 'react-native';
import { Block, Input, Button, Text } from 'galio-framework';
import styles from './login.styles';
import LoginPresenter from '../presenter/login.presenter';
import {Images} from './../../../../assets/Images';
const { height, width } = Dimensions.get('screen');

export default class LoginView extends React.Component {

    private loginPresenter = new LoginPresenter();

    render() {
        // @ts-expect-error
        const { navigation } = this.props;

        return (
            <Block flex style={styles.container}>
                <StatusBar barStyle="light-content" />
                <Block style={styles.containerIcon}>
                    <Image source={ Images.imageApp } style={styles.imageHeader}/>
                </Block>
                <Block style={styles.containerCard}>
                    <Text style={styles.textLabel}>¿Que tanto te conoces?</Text>
                    <Input placeholder="Usuario" />
                    <Input style={styles.input} placeholder="Clave" />
                    <Button style={styles.button} onPress={() => { this.loginPresenter.sendToLogin(navigation) }}>Iniciar Sesión</Button>
                    <Image source={ Images.imageLogo } style={styles.imageFooter}/>
                </Block>
            </Block>
        );
    }
}