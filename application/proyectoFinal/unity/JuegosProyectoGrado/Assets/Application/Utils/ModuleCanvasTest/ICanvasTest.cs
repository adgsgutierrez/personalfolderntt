﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICanvasTest
{
    void HideUIInterface();

    void LoadQuestions();

    void ShowUIInterface();
}
