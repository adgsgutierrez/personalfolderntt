package co.com.konrad.police_game.controller.auth;

import co.com.konrad.police_game.entity.Auth;
import co.com.konrad.police_game.entity.Response;
import org.springframework.http.ResponseEntity;

public interface IController {

    ResponseEntity<Response> getAuthorization(Auth input) throws Exception;
}
