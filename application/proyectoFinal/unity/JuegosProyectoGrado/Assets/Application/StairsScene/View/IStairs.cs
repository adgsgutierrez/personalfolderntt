﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStairs
{
   void ClickOnButton();
   void MovePlayerNextPosition();

   void MovePlayerToPortalFail(IPlayer active);

   void MovePlayerToPortalNext(IPlayer active);
}
