﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Questions
{
    public int id { get ; set ; }
    public string name { get ; set ; }
    public string type { get ; set ; }
    public string answer { get ; set ; }
    public string description { get ; set ; }
    public Options[] options { get ; set ; }
}
