export class Functions {

    public static orderAsc(p_array_json, p_key: string): any[] {
        return p_array_json.sort( (a, b) => {
           return a[p_key] > b[p_key];
        });
    }

    public static orderDesc(p_array_json, p_key: string): any[]{
        return Functions.orderAsc(p_array_json, p_key).reverse(); 
    }

    public static orderRandom(p_array_json, p_key: string): any[]{
        const rand = ( new Date().getSeconds() % 2);
        console.log('rand' , rand);
        if (rand) {
            return Functions.orderAsc(p_array_json, p_key);
        }
        return Functions.orderDesc(p_array_json, p_key)
    }
}