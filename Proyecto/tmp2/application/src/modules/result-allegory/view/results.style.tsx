import {  StyleSheet, Dimensions } from 'react-native';
import Theme from '../../../../assets/Theme';

const { width , height } = Dimensions.get('screen');

export default StyleSheet.create({
    container: {
      height: '100%',
      width: '100%'
    },
    textTitle: {
      fontSize: 33,
      width : width,
      textTransform: "uppercase",
      textAlign: "center",
      color: Theme.COLORS.GREEN
    },
    textSubTitle: {
      marginTop: 20,
      fontSize: 20,
      width : width,
      textAlign: "center",
      color: Theme.COLORS.GREEN
    },
  });