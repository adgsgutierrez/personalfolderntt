﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IPlayer
{
    private bool IsMovement = false;

    public ParticleSystem explosion;
    public ParticleSystem fire;
    private Vector3 destiny;
    private Vector3 towardsTarget;
    public AudioClip fail;
    public AudioClip portal;
    public AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
       audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsMovement)
        {
            towardsTarget = destiny - transform.position;
            Quaternion towardsTargetRotation = Quaternion.LookRotation (destiny , Vector3.up );
            //transform.rotation = Quaternion.Lerp(transform.rotation , towardsTargetRotation  , 1f * Time.deltaTime );
            transform.position +=  towardsTarget * 2f * Time.deltaTime;
        }
    }

    public void StartMoveAtField(Vector3 destinyInput)
    {
        IsMovement = true;
        destiny = destinyInput;
        
    }
    public void StopMoveAtField()
    {
        Debug.Log("Stop Movement Player");
        IsMovement = false;
        
    }



    public IEnumerator MoveAtPortal(Vector3 destiny)
    {
        fire.transform.position = transform.position;
        fire.Play();
        audio.clip = portal;
        audio.Play();
        yield return new WaitForSeconds(.3f);
        fire.Stop();
        transform.position = destiny;
    }
    public IEnumerator MoveAtFail(Vector3 destiny)
    {
        explosion.transform.position = transform.position;
        explosion.Play();
        audio.clip = fail;
        audio.Play();
        yield return new WaitForSeconds(.3f);
        explosion.Stop();
        transform.position = destiny;
    }

}
