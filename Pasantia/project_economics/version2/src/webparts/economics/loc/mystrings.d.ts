declare interface IEconomicsWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'EconomicsWebPartStrings' {
  const strings: IEconomicsWebPartStrings;
  export = strings;
}
