package co.com.konrad.police_game.controller.test;

import co.com.konrad.police_game.entity.Response;
import co.com.konrad.police_game.entity.Test;
import co.com.konrad.police_game.service.test.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/test")
public class TestController implements IController {

    @Autowired
    @Qualifier("TestServiceImpl")
    private IService service;

    @Override
    @PostMapping
        public Response setTest(@RequestBody Test test) {

        return service.save(test);
    }

}
