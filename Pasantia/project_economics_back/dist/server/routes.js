"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Routes = void 0;
const service_actual_1 = require("./../service/readxlsx/service.actual");
const service_geco_1 = require("./../service/readxlsx/service.geco");
exports.Routes = [
    {
        method: 'post',
        path: '/read/geco/:id',
        class: service_geco_1.ServiceGeco
    }, {
        method: 'post',
        path: '/read/actual',
        class: service_actual_1.ServiceActual
    }
];
//# sourceMappingURL=routes.js.map