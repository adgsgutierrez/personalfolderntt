import AsyncStorage from "@react-native-community/async-storage";
import { IAnswer, IAnswerSelected } from "../../../models/question.interface";
import { IResponse, IResponseConfiguration } from "../../../models/response.interface";
import { ITest } from "../../../models/test.interface";
import { IUser } from "../../../models/user.interface";
import Jwt from "../../../utilities/jwt";
import ServiceHttp from "../../../utilities/service";
import { Constants } from './../../../utilities/constants';

export default class ResultsModel {

    private service: ServiceHttp = new ServiceHttp();

    public getModel(input: string):IAnswerSelected {
        switch(input){
            case 'Agente': return require('./../../../../assets/src/Agente.json');
            case 'Alentador': return require('./../../../../assets/src/Alentador.json');
            case 'Consejero': return require('./../../../../assets/src/Consejero.json');
            case 'Creativo': return require('./../../../../assets/src/Creativo.json');
            case 'Especialista': return require('./../../../../assets/src/Especialista.json');
            case 'Evaluador': return require('./../../../../assets/src/Evaluador.json');
            case 'Investigador': return require('./../../../../assets/src/Investigador.json');
            case 'Objetivo': return require('./../../../../assets/src/Objetivo.json');
            case 'OrientadoResultados': return require('./../../../../assets/src/OrientadoResultados.json');
            case 'Perfeccionista': return require('./../../../../assets/src/Perfeccionista.json');
            case 'Persuasivo': return require('./../../../../assets/src/Persuasivo.json');
            case 'Profesional': return require('./../../../../assets/src/Profesional.json');
            case 'Promotor': return require('./../../../../assets/src/Promotor.json');
            case 'Realizador': return require('./../../../../assets/src/Realizador.json');
            case 'Resolutivo': return require('./../../../../assets/src/Resolutivo.json');
            default: return require('./../../../../assets/src/Default.json');
        }
    }

    public getArrayConfiguration(): IResponseConfiguration[]{
        return require('./../../../../assets/src/Response.json');
    }

    public saveInCloud(answer: IAnswer , total: number):Promise<void>{
        return new Promise(
            (resolve , reject) =>{
                if (!Constants.ISOFFLINE){
                    this.getInformationUser().then(
                        (user: IUser) => {
                            const test: ITest = {
                                test_id : null,
                                test_user: user.user_id,
                                test_create: new Date(),
                                test_update: new Date(),
                                test_total_c : ( answer.C.mas - answer.C.menos ),
                                test_total_d : (answer.D.mas - answer.D.menos ),
                                test_total_i : ( answer.I.mas - answer.I.menos ),
                                test_total_s: ( answer.S.mas - answer.S.menos ),
                                test_total:total
                            }
                            const host = Constants.HOST + Constants.PATH.TEST;
                            this.service.servicePost(host , test).then(
                                ( result: IResponse) => {
                                    console.log('result' , result);
                                    if (result.code === Constants.STATUS.SUCCESS) resolve 
                                    else reject
                                } , reject
                            ).catch (reject)
                        } , reject
                    ).catch (reject);
                }
                resolve();
            }
        )
    }

    private getInformationUser(): Promise<IUser>{
        return new Promise(
            (resolve , reject) => {
                AsyncStorage.getItem(Constants.PERSISTENCE.USER).then(
                    (data) => {
                        console.log('data ' , data);
                        const decode = Jwt.decode(data);
                        console.log('decode ' , decode);
                        const user: IUser = JSON.parse( decode['information'] );
                        resolve(user);
                    } , reject
                ).catch (reject);
            }
        );
    }
}