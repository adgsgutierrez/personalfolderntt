import React from 'react';
import { ScrollView, Text } from 'react-native';
import { Block } from 'galio-framework';;
import HeaderApplication from '../../../components/header/header.view';
import { IResults } from './iresults.view';
import ResultsPresenter from '../presenter/results.presenter';
import { IAnswerSelected } from '../../../models/question.interface';
import ItemResult from '../../../components/item-result/item_result.view';
import style from './results.style';

export default class Results extends React.Component implements IResults{

    private presenter: ResultsPresenter = new ResultsPresenter();
    private result:IAnswerSelected;

    constructor(prop){
        super(prop);
        console.log('data ' , this.props.route.params);
        const data = this.props.route.params.result;
        this.result  = this.presenter.searchProfile(data);
    }

    refreshData(): void { }

    render () {
        // @ts-expect-error
        const { navigation } = this.props;

        return (
        <Block flex>
             <HeaderApplication 
                    icon={require('./../../../../assets/images/alegoria.png')}
                    label="Alegoría"></HeaderApplication>
            <Text style={style.textSubTitle}>De acuerdo a lo que respondiste</Text>
            <Text style={style.textTitle}>Eres</Text>
            <Text style={style.textTitle}>{this.result.title}</Text>
            <ScrollView>
                { this.renderFeature() }
            </ScrollView>
        </Block>
        );
    }

    private renderFeature(): any[] {
        let fields = [];
        for (let i=0; i < this.result.features.length; i++) {
            const key = 'item_'+i;
            fields.push( <ItemResult key={key} title={this.result.features[i].title} description={this.result.features[i].description} /> );
        }
        return fields; 
    }
}