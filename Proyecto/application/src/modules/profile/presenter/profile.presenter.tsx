import AsyncStorage from "@react-native-community/async-storage";
import { IAnswer } from "../../../models/question.interface";
import { Constants } from "../../../utilities/constants";
import { Alert } from 'react-native';

export default class ProfilePresenter {

    public navigateToResults(navigation): void {
        AsyncStorage.getItem(Constants.PERSISTENCE.TEST).then(
            (data) => {
                if (data) { 
                    console.log('data' , data);
                    const result: IAnswer = JSON.parse(data);
                    navigation.navigate('Allegory/Results' , { result });
                } else {
                    Alert.alert('Completa la cartilla en el menú de Alegoría');
                }
            }
        )
    }

    public navigateToLogin(navigation): void {
        AsyncStorage.clear();
        navigation.replace('Login');
    }

    public saveInformation(): void {
        
    }
}