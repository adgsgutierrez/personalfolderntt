import { IAnswer, IAnswerSelected } from "../../../models/question.interface";
import { IResponseConfiguration } from "../../../models/response.interface";
import ResultsModel from "../model/result.model";

export default class ResultsPresenter {

    private resultModel: ResultsModel = new ResultsModel();

    private loadProfile( feature: string ): IAnswerSelected{
        return this.resultModel.getModel(feature);
    }

    public searchProfile( answer: IAnswer): IAnswerSelected {
        console.log('answer' ,answer);
        const feature = this.searchSegment(
            ( answer.D.mas - answer.D.menos ) , 
            ( answer.I.mas - answer.I.menos ) ,
            ( answer.S.mas - answer.S.menos ) ,
            ( answer.C.mas - answer.C.menos )
        );
        console.log('feature' ,feature);
        this.resultModel.saveInCloud(answer , feature);
        return this.searchProfileInConfig(feature);
    }

    private searchProfileInConfig(value): IAnswerSelected {
        let feature = '';
        const config: IResponseConfiguration[] = this.resultModel.getArrayConfiguration();
        for (let item of config) {
            if ( item.items.indexOf(value) > -1 ){
                feature = item.profile;
            }
        }
        return this.loadProfile(feature);
    }

    private searchSegment( D: number , I: number , S: number , C : number): number{
        let result = 0;
        // VARIABLE D
        if ( D >= 9 && D <= 27) { result = 7000; }
        else if ( D>=5 && D <= 8) { result = 6000; } 
        else if ( D>=2 && D <= 4) { result = 5000; } 
        else if ( D>=0 && D <= 1) { result = 4000; } 
        else if ( D>=-3 && D <= -1) { result = 3000; } 
        else if ( D>=-7 && D <= -4) { result = 2000; } 
        else { result = 1000; } 
        // VARIABLE I 
        if ( I >= 7 && I <=28) { result = result + 700; }
        else if ( I>=4 && I <= 6) { result = result + 600; } 
        else if ( I>=2 && I <= 3) { result = result + 500; } 
        else if ( I>=-1 && I <= 1) { result = result + 400; } 
        else if ( I>=-3 && I <= -2) { result = result + 300; } 
        else if ( I>=-7 && I <= -4) { result = result + 200; } 
        else { result = result + 100; } 
        // VARIABLE S
        if ( S >= 8 && S <=28) { result = result + 70; }
        else if ( S >=3 && S <= 7) { result = result + 60; } 
        else if ( S >=0 && S <= 2) { result = result + 50; } 
        else if ( S >=-3 && S <= -1) { result = result + 40; } 
        else if ( S >=-6 && S <= -4) { result = result + 30; } 
        else if ( S >=-10 && S <= -7) { result = result + 20; } 
        else { result = result + 10; } 
        // VARIABLE C
        if ( C >= 9 && C <=28) { result = result + 7; }
        else if ( C >=5 && C <= 8) { result = result + 6; } 
        else if ( C >=3 && C <= 4) { result = result + 5; } 
        else if ( C >=0 && C <= 2) { result = result + 4; } 
        else if ( C >=-2 && C <= -1) { result = result + 3; } 
        else if ( C >=-5 && C <= -3) { result = result + 2; } 
        else { result = result + 1; } 
        return result;
    }
}