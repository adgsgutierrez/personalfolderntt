﻿using System.Collections;
using UnityEngine;

public interface IPlayer
{
    void StartMoveAtField(Vector3 destiny);
    void StopMoveAtField();
    IEnumerator MoveAtPortal(Vector3 destiny);
    IEnumerator MoveAtFail(Vector3 destiny);
}
