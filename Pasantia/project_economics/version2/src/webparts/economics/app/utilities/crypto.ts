export class CryptoTls {

  private static BUCLES = 5;

  public static encode( input: any ): string {
    const stringInput: string = JSON.stringify(input);
    let stringDecode = encodeURI(stringInput);
    for (let index = 0 ; index < CryptoTls.BUCLES ; index++) {
      const randomLetter = CryptoTls.getRndInteger(65 , 90);
      const letter = String.fromCharCode(randomLetter);
      stringDecode = letter + btoa(stringDecode);
    }
    return stringDecode;
  }

  public static decode( input: string ): any {
    let stringDecode = input;
    for (let index = 0 ; index < CryptoTls.BUCLES ; index++) {
      stringDecode = atob(stringDecode.substr(1) );
    }
    stringDecode = decodeURI(stringDecode);
    return JSON.parse(stringDecode);
  }

  private static getRndInteger(min: number, max: number , integ: boolean = false ): number {
    let numb = Math.floor(Math.random() * (max - min) ) + min;
    if (integ) {
        // tslint:disable-next-line: radix
        numb = parseInt( '' + numb);
    }
    return numb;
  }

}
