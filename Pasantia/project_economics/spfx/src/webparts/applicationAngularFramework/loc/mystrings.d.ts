declare interface IApplicationAngularFrameworkWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'ApplicationAngularFrameworkWebPartStrings' {
  const strings: IApplicationAngularFrameworkWebPartStrings;
  export = strings;
}
