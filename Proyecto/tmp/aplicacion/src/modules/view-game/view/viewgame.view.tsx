import React from 'react';
import { Alert , Text , NativeModules } from 'react-native';
import { Block } from 'galio-framework';
import HeaderApplication from '../../../components/header/header.view';
import ViewGamePresenter from '../presenter/viewgame.presenter';

export default class ViewGame extends React.Component{

    private presenter: ViewGamePresenter = new ViewGamePresenter();

    componentDidMount() {
        NativeModules.GameModule.NavigateToGame();    
    }

    render () {
        // @ts-expect-error
        const { navigation } = this.props;

        return (
        <Block flex>
             <HeaderApplication 
                    icon={require('./../../../../assets/images/game.png')}
                    label="Habilidad"></HeaderApplication>
                <Text>Loading...</Text>
        </Block>
        );
    }
}