package co.com.konrad.police_game.service.auth;

import co.com.konrad.police_game.entity.Auth;
import co.com.konrad.police_game.entity.Response;

public interface IService {

    Response getAuthorization(Auth input) throws Exception;
}
