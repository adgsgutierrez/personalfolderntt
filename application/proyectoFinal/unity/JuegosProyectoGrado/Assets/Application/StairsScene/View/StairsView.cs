﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StairsView : MonoBehaviour, IStairs
{
    private List<GameObject> table = new List<GameObject>();
    private int move;
    private IEnumerator coroutine;
    private bool IsMovement;
    public GameObject player;
    private GameObject field;
    public StairsPresenter stair = new StairsPresenter();
    private bool IsSearchNumber;
    public Text carcel;
    public Text lanzamientos;
    public Text numbers;
    public Text points;
    private int point;
    public Button button;
    private int playerLaunch;
    private int playerFail;
        public GameObject Question;

    // Start is called before the first frame update
    void Start()
    {
        playerLaunch = 0;
        playerFail = 0;
        point = 0;
        lanzamientos.text = "Lanzamientos: " + playerLaunch;
        carcel.text = "En la Cárcel: " + playerFail;
        numbers.text = "";
        points.text = "" + point + " Puntos";
        IsSearchNumber = false;
        // Load table
        for (int indexX = 1 ;  indexX < 12 ; indexX ++) {
            for (int indexY = 1 ;  indexY < 6 ; indexY ++) {
                string tag = "row_" +indexX + "_colum_" + indexY;
                table.Add( GameObject.Find(tag) );
            }
        }
        IsMovement = false;
        // Player
        player = GameObject.Find("TT_demo_police");
    }

    // Update is called once per frame
    void Update()
    {
        if (IsMovement){
            var position = field.transform.position;
            IPlayer active = player.GetComponent( typeof(IPlayer) ) as IPlayer;
            active.StartMoveAtField(field.transform.position);
        }
        if (field != null){
            if(Vector3.Distance(player.transform.position, field.transform.position) < 0.1f){
                MovePlayerNextPosition();
            }
        }
        if (IsSearchNumber)
        {
            numbers.text = "" + stair.RunRandom();
        }
    }

    public void ClickOnButton()
    {
        StartCoroutine("RunNumber");
    }
   
    private IEnumerator RunNumber()
    {
        IsSearchNumber = true;
        button.interactable = false;
        yield return new WaitForSeconds(.5f);
        IsSearchNumber = false;
        playerLaunch = playerLaunch + 1;
        lanzamientos.text = "Lanzamientos: " + playerLaunch;
        int numberNext = stair.RunRandom();
        numbers.text = "" + numberNext;
        move = stair.SetPosition(numberNext);
        field = table[move];
        IsMovement = true;
        button.interactable = true;
        SendQuestion();
    }

    public void MovePlayerNextPosition()
    {
        Debug.Log("Finish movement");
        point = point + 50;
        IPlayer active = player.GetComponent( typeof(IPlayer) ) as IPlayer;
        Box boxScript = field.GetComponent( typeof(Box) ) as Box;
        active.StopMoveAtField();
        if(boxScript.isFail){
            point = (point > 200) ? point - 200 : 0;
            Debug.Log("Movement isFail");
            this.MovePlayerToPortalFail(active);
        }
        if(boxScript.isPortal){
            Debug.Log("Movement isPortal");
            point = point + 200;
            this.MovePlayerToPortalNext(active);
        }
        IsMovement = false;
        field = null;
        points.text = "" + point + " Puntos";
    }

    public void MovePlayerToPortalFail(IPlayer active)
    {
        Vector3 positionFail = table[0].transform.position;
        for (int index = ( move - 1 ) ; index > 0 ; index --) {
            GameObject objTmp = table[index];
            Box boxScript = objTmp.GetComponent( typeof(Box) ) as Box;
            if (boxScript.isFail) {
                move = index;
                index = 0;
                positionFail = table[move].transform.position;
            }
        }
        stair.SetPositionForce(move);
        IEnumerator routine = active.MoveAtFail(positionFail);
        playerFail = playerFail + 1;
        carcel.text = "En la Cárcel: " + playerFail;
        StartCoroutine(routine);
    }
    public void MovePlayerToPortalNext(IPlayer active)
    {
        Vector3 positionPortal = player.transform.position;
        for (int index = ( move + 1 ) ; index < table.Count ; index ++) {
            GameObject objTmp = table[index];
            Box boxScript = objTmp.GetComponent( typeof(Box) ) as Box;
            if (boxScript.isPortal) {
                move = index;
                index = table.Count;
                positionPortal = table[move].transform.position;
            }
        }
        stair.SetPositionForce(move);
        IEnumerator routine = active.MoveAtPortal(positionPortal);
        StartCoroutine(routine);
    }

    private void SendQuestion(){
        ICanvasTest active = Question.GetComponent( typeof(ICanvasTest) ) as ICanvasTest;
        active.ShowUIInterface();
    }

}
