﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pironola : MonoBehaviour
{
    static float speed = 80.1f;
    public GameObject pirinola;
    private Vector3 position;
    private GameObject context;
    public GameObject Question;
    private Rigidbody rb;
    private bool isRotate;

    // Start is called before the first frame update
    void Start()
    {
        position = pirinola.transform.position;
        //context = GameObject.Find("perinolanumerica");
        rb = pirinola.GetComponent<Rigidbody>();
        isRotate = false;
    }

    // Update is called once per frame
    void Update()
    {   
        
    }

    

    private void SendQuestion(){
        ICanvasTest active = Question.GetComponent( typeof(ICanvasTest) ) as ICanvasTest;
        active.ShowUIInterface();
    }

    public void RunPironola()
    {
        //
        isRotate = true;
        pirinola.transform.position = position;
        pirinola.transform.rotation = Quaternion.Euler( -90f,  -1f,  0f);
        rb.AddTorque(transform.up * speed * 14f);

    }

    void FixedUpdate(){
        float mg = rb.angularVelocity.magnitude;
        if (mg == 0f && isRotate) {
            SendQuestion();
            isRotate = false;
            Debug.Log("pirinola.transform.rotation " + mg);
        }
    }
}
