﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour, IShoot
{

    public GameObject ball;
    private Vector3 throwSpeed = new Vector3(0, 13, 20); 
    public GameObject arrow;
    private float arrowSpeed = 0.05f;
    private bool right = true;
    private bool thrown = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        /* Move Meter Arrow */
        ViewArrow();
    }


    public void ViewArrow()
    {
        if (arrow.transform.position.x < 1.5f && right)
        {
            arrow.transform.position += new Vector3(arrowSpeed, 0, 0);
        }
        if (arrow.transform.position.x >= 1.5f)
        {
            right = false;
        }
        if (right == false)
        {
            arrow.transform.position -= new Vector3(arrowSpeed, 0, 0);
        }
        if ( arrow.transform.position.x <= -1.5f)
        {
            right = true;
        }
    }

    public void StartShot(){
        thrown = true;
        throwSpeed.y = throwSpeed.y + arrow.transform.position.x;
        throwSpeed.z = throwSpeed.z + arrow.transform.position.x;
        ball.GetComponent<Rigidbody>().AddForce(throwSpeed, ForceMode.Impulse);
        ball.gameObject.GetComponent<AudioSource>().Play(0);
    }
    
    void FixedUpdate(){
    if (Application.platform == RuntimePlatform.Android)
    {
    if (Input.GetKey(KeyCode.Escape))
    {
        Application.Quit();
    }
    }
    }
}
