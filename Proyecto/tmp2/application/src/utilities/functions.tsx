import Base64 from "./base64";

export class Functions {

    public static orderAsc(p_array_json, p_key: string): any[] {
        return p_array_json.sort( (a, b) => {
           return a[p_key] > b[p_key];
        });
    }

    public static orderDesc(p_array_json, p_key: string): any[]{
        return Functions.orderAsc(p_array_json, p_key).reverse(); 
    }

    public static orderRandom(p_array_json, p_key: string): any[]{
        const rand = ( new Date().getSeconds() % 2);
        console.log('rand' , rand);
        if (rand) {
            return Functions.orderAsc(p_array_json, p_key);
        }
        return Functions.orderDesc(p_array_json, p_key)
    }

    public static encriptData( value: string ): string {
        const INTERATION_ENCRIPTION = 2;
        let head = Base64.btoa( encodeURI ( value) );
        for (let index = 0 ; index < INTERATION_ENCRIPTION ; index ++) {
            const random = Functions.getRndInteger(65 , 90 , false);
            head = Base64.btoa(( String.fromCharCode(random) + head ) );
        }
        return head;
    }

    public static getRndInteger = (min, max, integ) => {
        let numb = Math.floor(Math.random() * (max - min)) + min;
        if (integ) {
            numb = parseInt(numb);
        }
        return numb;
    }

}