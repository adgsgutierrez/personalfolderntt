package com.everis.economics.start;

import com.everis.economics.beans.ConfigurationUploadBean;
import com.everis.economics.controller.ControllerSaveFile;
import com.everis.economics.service.ServiceSaveFile;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = ControllerSaveFile.class)
@ComponentScan(basePackageClasses = ServiceSaveFile.class)
@ComponentScan(basePackageClasses = ConfigurationUploadBean.class)
public class EconomicsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EconomicsApplication.class, args);
	}

}
