import React from 'react';
import { Image, View } from 'react-native';
import { Block, Input, Button } from 'galio-framework';
import HeaderApplication from '../../../components/header/header.view';
import styles from './profile.style';
import { Images } from '../../../../assets/Images';
import ItemMenu from '../../../components/item-menu/item_menu.view';
import { ScrollView } from 'react-native-gesture-handler';
import ProfilePresenter from '../presenter/profile.presenter';
import { IUser } from '../../../models/user.interface';
import { Constants } from '../../../utilities/constants';
import AsyncStorage from '@react-native-community/async-storage';
import Jwt from '../../../utilities/jwt';

export default class Profile extends React.Component {

    private presenter: ProfilePresenter = new ProfilePresenter();

    constructor(props) {
        super(props);
        this.state = { name : '', lastname : '' , range : ''  , gender: ''  };
        this.readData();
    }

    private readData():void{
        let state = this;
        AsyncStorage.getItem(Constants.PERSISTENCE.USER).then(
            (data) => {
                console.log('data ' , data);
                const decode = Jwt.decode(data);
                console.log('decode ' , decode);
                const user: IUser = JSON.parse( decode['information'] );
                console.log('user ' , user);
                state.setState({ name : user.user_name, lastname : user.user_lastname, range : user.user_range , gender: user.user_gender });
            }
        );
        
        
    }

    render() {
        const { navigation } = this.props;
        return (
            <View>
                <HeaderApplication
                    icon={require('./../../../../assets/images/profile.png')}
                    label="Perfil"></HeaderApplication>
                <ScrollView>
                    <View style={styles.containerImage}>
                        <Image style={styles.imageHeader} source={
                            (this.state.gender === 'F') ? require('./../../../../assets/images/avatar-women.png') : require('./../../../../assets/images/avatar-man.png')
                        } />
                    </View>
                    <Block style={styles.containerCard}>
                        <Input value={this.state.name} placeholder="Nombre" />
                        <Input value={this.state.lastname} placeholder="Apellido" />
                        <Input value={this.state.range} placeholder="Rango" />
                    </Block>
                    <View style={styles.containerButtons}>
                        <ItemMenu 
                            navigateTo={ () => { this.presenter.navigateToResults(navigation) } } 
                            icon={require('./../../../../assets/images/alegoria.png')}
                            label="Ver Resultados">
                        </ItemMenu>
                        <ItemMenu
                            navigateTo={ () => { this.presenter.navigateToLogin(navigation) } } 
                            icon={require('./../../../../assets/images/cerrar_session.png')}
                            label="Cerrar Sesión">
                        </ItemMenu>
                        <Image source={ Images.imageLogo } style={styles.imageFooter}/>
                    </View>
                </ScrollView>
            </View>
        );
    }
}