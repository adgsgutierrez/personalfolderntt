export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCFwS0J589LF1MN2szA-21M9h2K1eQ9wy4',
    authDomain: 'demotl-s.firebaseapp.com',
    databaseURL: 'https://demotl-s.firebaseio.com',
    projectId: 'demotl-s',
    storageBucket: 'demotl-s.appspot.com',
    messagingSenderId: '659679152070',
    appId: '1:659679152070:web:69fb948c351830dcc954c7'
  },
  session : {
    data : 'BJKSLORP'
  },
  menu: {
    login : 'login',
    error: 'error',
    home : 'table-control/home',
    tls : 'table-control/tls',
    profile: 'table-control/profiles'
  }
};
